--:setvar master "master"

USE [$(master)]

-- reconfigure can't be run when the database is in a transition.
-- therefore we will only run it if we really need to

IF((SELECT CONVERT(int, value_in_use) FROM sys.configurations WHERE name='allow updates') <> 0)
BEGIN
	EXEC sp_configure 'allow updates', 0
	RECONFIGURE
END

GO

IF NOT EXISTS (SELECT sysobjects.name FROM sysobjects WHERE sysobjects.name = 'clearJobStatus' AND xtype = 'P') 
	EXECUTE sp_executesql N'create procedure clearJobStatus as begin
		update msdb.dbo.sysjobactivity set stop_execution_date = GETDATE() where 
		start_execution_date is not null and stop_execution_date is null
		end'
GO

sp_procoption @ProcName = 'clearJobStatus', 
              @OptionName = 'startup', 
              @OptionValue = 'on'
GO

IF((SELECT CONVERT(int, value_in_use) FROM sys.configurations WHERE name='show advanced options') <> 1)
BEGIN
	EXEC sp_configure 'show advanced options', 1;
	RECONFIGURE
END
GO

IF((SELECT CONVERT(int, value_in_use) FROM sys.configurations WHERE name='Ad Hoc Distributed Queries') <> 1)
BEGIN
	EXEC sp_configure 'Ad Hoc Distributed Queries', 1;
	RECONFIGURE
END
GO
