--:setvar master "master"

IF NOT EXISTS (SELECT * FROM sys.syslogins WHERE name = 'sa')
BEGIN
CREATE LOGIN [sa] WITH PASSWORD=N'RetailInsight123$', DEFAULT_DATABASE=[$(master)], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=ON

ALTER SERVER ROLE [sysadmin] ADD MEMBER [sa]
END
