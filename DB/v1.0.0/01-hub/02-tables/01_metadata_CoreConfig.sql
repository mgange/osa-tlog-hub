IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[metadata].[CoreConfig]') AND type in (N'U'))
DROP TABLE [metadata].[CoreConfig]
GO

CREATE TABLE [metadata].[CoreConfig](
	[DBName] [nvarchar](64) NOT NULL,
	[ConfigName] [nvarchar](128) NOT NULL,
	[ConfigDescription] [nvarchar](256) NULL,
	[ConfigValue] [nvarchar](max) NULL,
	[Category] [nvarchar](64) NULL,
CONSTRAINT [PK_CORE_CONFIG] PRIMARY KEY CLUSTERED 
(
	[DBName] ASC,
	[ConfigName] ASC
)	
) ON [PRIMARY]

GO