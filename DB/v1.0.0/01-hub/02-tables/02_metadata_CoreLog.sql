IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[etl].[CoreLog]') AND type in (N'U'))
CREATE TABLE [etl].[CoreLog] (
	[LogID] [bigint] NOT NULL IDENTITY (100,1),
	[SourceType][nvarchar] (128),
	[SourceKey][bigint],
	[Description] [nvarchar](4000),
	[Status] [nvarchar] (128),
	[CreateDate] [datetime],
	[DBName] [nvarchar] (64),
 CONSTRAINT [PK_LOG] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
)
) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[etl].[CoreLog]') AND name = N'IX_Log1')
CREATE NONCLUSTERED INDEX IX_Log1 ON [etl].[CoreLog] 
([DBName] ASC,[SourceType] ASC,[CreateDate] ASC) INCLUDE ([Status])  ON [PRIMARY]
GO

-- create index for rsi_log
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[etl].[CoreLog]') AND name = N'IX_Log2')
CREATE NONCLUSTERED INDEX IX_Log2 ON [etl].[CoreLog] 
([SourceType] ASC,[Status] ASC,[CreateDate] ASC) INCLUDE ([DBName]) ON [PRIMARY]
GO