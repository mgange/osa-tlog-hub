------------------ Table Definition Starts Here --------------------

-- Configuration to connect to a remote server (both for picking up files and uploading files)
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'metadata.FilemoverServer') AND type in (N'U'))
CREATE TABLE metadata.FilemoverServer
(
	ServerName				nvarchar(512) NOT NULL,
	ServerType				nvarchar(10) NOT NULL,
	ServerAddress			nvarchar(512) NULL,
	PortNo					int NULL,
	Username				nvarchar(512) NULL,
	Password				nvarchar(512) NULL,
	RootDirectory			nvarchar(512) NULL,
	UploadDirectory			nvarchar(512) NULL,
	InprogressUploadDirectory  nvarchar(512) NULL,
	Rename					bit NOT NULL DEFAULT 0,
	Active					bit NOT NULL DEFAULT 1,
	Recursive 				bit NOT NULL DEFAULT 1,
	CreateDate				DATETIME NOT NULL DEFAULT getdate(),
	UpdateDate				DATETIME NOT NULL DEFAULT getdate(),
	CreatedBy				nvarchar(512) NOT NULL DEFAULT SUSER_NAME(),
	UpdatedBy				nvarchar(512) NOT NULL DEFAULT SUSER_NAME()
 PRIMARY KEY CLUSTERED(ServerName)
);

-- Configure which files should be moved from SOURCE_SERVER to TARGET_SERVER
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'metadata.FilemoverMapping') AND type in (N'U'))
CREATE TABLE metadata.FilemoverMapping
(
	SourceServerName		nvarchar(512) NOT NULL,
	SourcePath				nvarchar(512) NOT NULL,
	TargetServerName		nvarchar(512) NOT NULL,
	FilePattern				nvarchar(512) NOT NULL,
	KeepSubFolder			bit NOT NULL,
	DeleteSource			bit NOT NULL,
	Compress				bit NOT NULL DEFAULT(0),
	Priority				int NOT NULL,
	BackupFolder			nvarchar(512) NOT NULL,
	BackupDaysToKeep		int NULL,
	CreateDate				DATETIME NOT NULL DEFAULT getdate(),
	UpdateDate				DATETIME NOT NULL DEFAULT getdate(),
	CreatedBy				nvarchar(512) NOT NULL DEFAULT SUSER_NAME(),
	UpdatedBy				nvarchar(512) NOT NULL DEFAULT SUSER_NAME()
 PRIMARY KEY CLUSTERED( SourceServerName,SourcePath,TargetServerName,FilePattern)
);
-- List of files available for movement, populated by FileMover but can also be populated from 3rd party (push instead of pull)
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'etl.FilemoverAvailableFiles') AND type in (N'U'))
CREATE TABLE etl.FilemoverAvailableFiles
(
	FileID					int NOT NULL IDENTITY(1,1),
	ServerName				nvarchar(512) NOT NULL,
	SourcePath				nvarchar(512) NOT NULL,
	FullPath				nvarchar(512) NOT NULL,
	FileSize				BIGINT NOT NULL,
	FileLastModified		datetime NOT NULL,
	CreateDate				datetime NOT NULL DEFAULT getdate(),
	UpdateDate				datetime NOT NULL DEFAULT getdate(),
	CreatedBy				nvarchar(512) NOT NULL DEFAULT SUSER_NAME(),
	UpdatedBy				nvarchar(512) NOT NULL DEFAULT SUSER_NAME()
 PRIMARY KEY CLUSTERED(FileID)
);
-- One record for every file that gets moved. Note that if one file is moved to several locations there would be several records with the same FILE_ID
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'etl.FilemoverStatus') AND type in (N'U'))
CREATE TABLE etl.FilemoverStatus
(
	FileID					int NOT NULL,
	TargetServerName		nvarchar(512) NOT NULL,
	Status					nvarchar(64) NOT NULL DEFAULT 'IN PROGRESS',
	StatusReason			nvarchar(1024) NULL,
	CreateDate				datetime NOT NULL DEFAULT getdate(),
	UpdateDate				datetime NOT NULL DEFAULT getdate(),
	CreatedBy				nvarchar(512) NOT NULL DEFAULT SUSER_NAME(),
	UpdatedBy				nvarchar(512) NOT NULL DEFAULT SUSER_NAME()
 PRIMARY KEY CLUSTERED( FileID,TargetServerName)
);

