IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'etl.csp_AddLog') AND type in (N'P', N'PC'))
  DROP PROCEDURE etl.[csp_AddLog]
GO

CREATE PROCEDURE etl.[csp_AddLog]
      -- Add the parameters for the stored procedure here
      @pvs_source_type         VARCHAR(100),
      @pvn_source_key          BIGINT,
      @pvs_description        VARCHAR(3800),
      @pvs_status             VARCHAR(100) = 'INFO'
AS
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;

      DECLARE @lvn_source_key    BIGINT,
              @lvs_source_type   VARCHAR(100),
              @lvs_dbname      VARCHAR(100),
              @lvs_err_msg      NVARCHAR(4000),
              @lvn_err_sev      INT,
              @lvn_err_state    INT;

   BEGIN TRY
         -- get silo_id, make sure we get only one row
      SET @lvs_dbname = '$(dbName)'

      IF @lvs_dbname IS NULL
         RAISERROR('Unable to determine dbname',16,1);

      IF @pvs_status = 'COMPLETE'
      BEGIN
      	SET @pvs_status = 'COMPLETED'
      END

            -- add log
      INSERT INTO etl.CoreLog
                  (SourceType,
                  SourceKey,
                  Description,
                  Status,
                  CreateDate,
                  DBName)
            VALUES(@pvs_source_type,
                  @pvn_source_key,
                  @pvs_description,
                  @pvs_status,
                  GETDATE(),
                  @lvs_dbname);


   END TRY
   BEGIN CATCH
      if @@trancount > 0
         ROLLBACK;
      SELECT @lvs_err_msg = ERROR_MESSAGE(),
             @lvn_err_sev = ERROR_SEVERITY(),
             @lvn_err_state = ERROR_STATE();
      RAISERROR (@lvs_err_msg, @lvn_err_sev, @lvn_err_state);
   END CATCH

END

GO