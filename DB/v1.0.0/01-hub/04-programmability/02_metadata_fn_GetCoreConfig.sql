IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'metadata.fn_GetCoreConfig') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION metadata.[fn_GetCoreConfig]
GO


CREATE FUNCTION metadata.[fn_GetCoreConfig]
(
  -- Add the parameters for the function here
  @pvs_name nvarchar(128)
)
RETURNS nvarchar(max)
AS
BEGIN
  -- Declare the return variable here
  DECLARE @pvs_value nvarchar(max)

  SELECT @pvs_value = ConfigValue
  FROM   [metadata].[CoreConfig]
  WHERE  ConfigName = @pvs_name

    declare @value nvarchar(max)
    declare @part1 nvarchar(max)
    declare @part2 nvarchar(max)

    declare @idxs int
    declare @idxe int
    declare @len int

    set @idxs = CHARINDEX('${', @pvs_value);

    while @idxs > 0
  begin

    set @len = LEN(@pvs_value)
    set @idxs = @idxs+2
    set @idxe = CHARINDEX('}', @pvs_value, @idxs);
    set @pvs_name = SUBSTRING(@pvs_value,@idxs,(@idxe-@idxs));

    set @value = (SELECT ConfigValue from [metadata].[CoreConfig] where ConfigName=@pvs_name)

    if @value is null
      set @value = ''

    if @idxs > 3
      set @part1 = Left(@pvs_value, @idxs-3)
    else
      set @part1 = ''
    if @idxe < @len
      set @part2 = RIGHT(@pvs_value, @len-@idxe)
    else
      set @part2 = ''

    set @pvs_value = @part1 + @value + @part2;
    set @idxs = CHARINDEX('${', @pvs_value);
  end

  -- Return the result of the function
  RETURN @pvs_value

END
GO