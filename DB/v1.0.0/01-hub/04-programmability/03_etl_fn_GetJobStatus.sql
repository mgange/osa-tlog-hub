IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'etl.fn_GetJobStatus') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION etl.fn_GetJobStatus
GO

CREATE FUNCTION etl.fn_GetJobStatus
	(
		@JOB_NAME NVARCHAR(MAX)
	)
	RETURNS NVARCHAR(50)
	AS
	BEGIN
		DECLARE @JOB_STATUS NVARCHAR(50)
		DECLARE @JOB_ID NVARCHAR(MAX)

		SELECT @JOB_ID = job_id
		FROM [msdb].[dbo].[sysjobs]
		WHERE name = @JOB_NAME

		SELECT @JOB_STATUS = LastRunStatus
		FROM
		(
			SELECT 
			CASE [sJOBH].[run_status]
				WHEN 0 THEN 'Failed'
				WHEN 1 THEN 'Succeeded'
				WHEN 2 THEN 'Retry'
				WHEN 3 THEN 'Canceled'
				WHEN 4 THEN 'Running'
			END AS [LastRunStatus]
			FROM
			(
				SELECT [run_status],
					   ROW_NUMBER() OVER (PARTITION BY [job_id] ORDER BY [run_date] DESC, [run_time] DESC) AS RowNumber
				FROM [msdb].[dbo].[sysjobhistory]
				WHERE [job_id] = @JOB_ID
				AND [step_id] = 0
			) AS [sJOBH]
			WHERE [sJOBH].[RowNumber] = 1
		) AS JOB

		RETURN @JOB_STATUS 

	END 