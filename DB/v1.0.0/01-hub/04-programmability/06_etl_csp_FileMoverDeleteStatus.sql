IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'etl.[csp_FileMoverDeleteStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE etl.csp_FileMoverDeleteStatus
GO

CREATE PROCEDURE etl.csp_FileMoverDeleteStatus
      @fileIDs        VARCHAR(512)
AS
BEGIN
	DECLARE @sql varchar(max)
	IF(@fileIDs is null)
		RAISERROR ('fileIDs cannot be NULL!', 16, -1);
	ELSE
	BEGIN
		SET @sql = 'DELETE FROM etl.FileMoverStatus WHERE FileID IN (' + @fileIDs + ')'
		EXEC(@sql)
		IF(@@ROWCOUNT > 0)
			EXEC etl.[csp_AddLog] 'RECOVERY', -9999, @sql, 'COMPLETE'

	END
END

GO