IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'etl.csp_FileMoverAuditIgnore') AND type in (N'P', N'PC'))
DROP PROCEDURE etl.csp_FileMoverAuditIgnore
GO

CREATE PROCEDURE etl.csp_FileMoverAuditIgnore
      @loadIDs        VARCHAR(512)
AS
BEGIN
	DECLARE @sql varchar(max), @msg varchar(max)
	IF(@loadIDs is null)
		RAISERROR ('loadIDs cannot be NULL!', 16, -1);
	ELSE
	BEGIN
		SET @sql = 'UPDATE etl.FileMoverAudit SET Status = ''IGNORE'', StatusReason = ISNULL(STATUS_REASON,'''') + ''/Manually set to IGNORE''
					WHERE LOAD_ID IN (' + @loadIDs + ')'
		EXEC(@sql)
		IF(@@ROWCOUNT > 0)
		BEGIN
			SET @msg = REPLACE(@sql, '''', '*')
			EXEC etl.csp_AddLog 'RECOVERY', -9999, @msg, 'COMPLETE'
		END

	END
END

GO