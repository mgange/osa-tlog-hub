IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_SplitStrings]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_SplitStrings]
GO
CREATE FUNCTION [dbo].[fn_SplitStrings]
(
   @List       NVARCHAR(MAX),
   @Delimiter  NVARCHAR(255)
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
(
  SELECT Item = y.i.value('(./text())[1]', 'nvarchar(4000)')
  FROM
  (
	SELECT x = CONVERT(XML, '<i>'
	  + REPLACE(@List, @Delimiter, '</i><i>')
	  + '</i>').query('.')
  ) AS a CROSS APPLY x.nodes('i') AS y(i)
);
GO