IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'etl.csp_FileMoverSetStatus')
                                      AND type in (N'P', N'PC'))
  DROP PROCEDURE etl.csp_FileMoverSetStatus
GO
CREATE PROCEDURE etl.csp_FileMoverSetStatus
      @FileIDs        VARCHAR(512) , @Status VARCHAR(512)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @sql varchar(max), @msg varchar(max)
	IF(@FileIDs is null)
		RAISERROR ('FileIDs cannot be NULL!', 16, -1);
	ELSE
	BEGIN

		SET @sql = 'UPDATE [etl].[FilemoverAudit] SET Status = '+ @Status +', StatusReason = ISNULL(StatusReason,'''') + ''/Manually set to IGNORE''
					WHERE FileID IN (' + @FileIDs + ')'
		EXEC(@sql)
		IF(@@ROWCOUNT > 0)
		BEGIN
			SET @msg = REPLACE(@sql, '''', '*')
			EXEC etl.[csp_AddLog] 'RECOVERY', -9999, @msg, 'COMPLETE'
		END

	END
END
GO