--:setvar BuildNo "POC"
--:setvar dbName "HUB_POC"
--:setvar deployDir "C:\RI\osa-tlog-hub"
--:setvar dbServer ".\SQL2014"
--:setvar PortNo: "1433"

DELETE FROM  metadata.CoreConfig;

INSERT INTO metadata.CoreConfig (DBName,ConfigName,ConfigValue,Category)
SELECT '$(dbName)' AS DBName,'build.number' AS PROPERTY_NAME,'$(BuildNo)' AS PROPERTY_VALUE,'deploy' AS CATEGORY
UNION SELECT '$(dbName)','deploy.database.id','$(dbName)','deploy'
UNION SELECT '$(dbName)','etl.database.type','HUB','deploy'
UNION SELECT '$(dbName)','db.database.name','$(dbName)','db'
UNION SELECT '$(dbName)','db.server.name','$(dbServer)','db'
UNION SELECT '$(dbName)','db.port.no','$(PortNo)','db'

UNION SELECT '$(dbName)','deploy.metadata.dir','$(deployDir)\Metadata\$(dbName)','deploy'
UNION SELECT '$(dbName)','deploy.etl.dir','$(deployDir)\etl','deploy'

UNION SELECT '$(dbName)','etl.filemover.retry','5','etl'
UNION SELECT '$(dbName)','etl.filemover.retrytimegap','12','etl'
UNION SELECT '$(dbName)','etl.logs.dir','$(deployDir)\etl\logs','etl'




