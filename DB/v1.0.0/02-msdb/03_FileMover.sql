--:setvar dbName "HUB_POC"
--:setvar deployDir "C:\RI\osa-tlog-hub"
--:setvar dbServer "RIL-MGANGE\sql2014"
--:setvar ProxyAccount ""

IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'$(dbName)#FileMover')
	EXEC msdb.dbo.sp_delete_job @job_name = N'$(dbName)#FileMover', @delete_unused_schedule=1;
GO

BEGIN
	BEGIN TRANSACTION
	DECLARE @ReturnCode INT
	SELECT @ReturnCode = 0

	IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[$(dbName)]' AND category_class=1)
	BEGIN
	EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[$(dbName)]'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

	END

	DECLARE @jobId BINARY(16)
	EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'$(dbName)#FileMover',
			@enabled=0,
			@notify_level_eventlog=0,
			@notify_level_email=2,
			@notify_level_netsend=0,
			@notify_level_page=0,
			@delete_level=0,
			@description=N'No description available.',
			@category_name=N'[$(dbName)]',
			@notify_email_operator_name=N'Support',
			@owner_login_name=N'sa',
			@job_id = @jobId OUTPUT
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'FileMover',
			@step_id=1,
			@cmdexec_success_code=0,
			@on_success_action=1,
			@on_success_step_id=0,
			@on_fail_action=2,
			@on_fail_step_id=0,
			@retry_attempts=0,
			@retry_interval=0,
			@os_run_priority=0, @subsystem=N'CmdExec',
			@command=N'powershell "$(deployDir)\etl\FileMover\main.ps1" -dbName "$(dbName)" -dbServer "$(dbServer)"',
			@flags=0,
			@proxy_name=N'$(ProxyAccount)'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

	EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'FileMover',
			@enabled=1,
			@freq_type=4,
			@freq_interval=1,
			@freq_subday_type=4,
			@freq_subday_interval=5,
			@freq_relative_interval=0,
			@freq_recurrence_factor=0,
			@active_start_date=20120331,
			@active_end_date=99991231,
			@active_start_time=0,
			@active_end_time=235959
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

	EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	COMMIT TRANSACTION
	GOTO EndSave
	QuitWithRollback:
		IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
	EndSave:
END

GO