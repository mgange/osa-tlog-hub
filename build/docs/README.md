## Getting Started

* Add this repository as a submodule of the project you want to build
* Download the latest DB Deployment Utility from the downloads section of the db-deploy-util Bitbucket repo  [dbdeploy.exe](https://bitbucket.org/Retail_Development/db-deploy-util/downloads/dbdeploy.exe)
* Put dbdeploy.exe in ./build/tools
* If you are cloning from sourcetree, the submoules will initialise automatically
* If you are clonfing from the command line, initialise the submodules
```
cd /path/to/this/repo
git submodule init
```

## Development TL;DR;

To get a deployed osa-crossmark up on your local instance ASAP.

1. Open Powershell
2. cd /path/to/repo/build/tools
3. .\build.ps1

For more information navigate to [docs](https://bitbucket.org/Retail_Development/build-tools/src/master/build/docs/)

## Tooling

### SQL Version

This solution currently targets SQL2014 and SSDT2014 and Powershell 5.
In addition, SSIS package deployment is supported, but not project deployment. This will be fixed in CMK-20.

SSDT2013 (for SQL2014) (https://msdn.microsoft.com/dn864412)
SSDTBI2013 (for SQL2014) (https://www.microsoft.com/download/details.aspx?id=42313)

### Powershell

PS5 (https://www.microsoft.com/en-au/download/details.aspx?id=48729)

If you have never used Powershell before, you need to tell Powershell that it is ok to run unsigned scripts locally.
1. Open Powershell as Administrator
2. Execute the following (only needs to be done once)
```
Set-ExecutionPolicy RemoteSigned
```

### dbdeploy

DbDeploy is a utility based on [DbUp](https://dbup.readthedocs.org/en/latest/).

It has been modified from its original form to support multi-db deployments, hence the need for the strict folder naming conventions.

You can Download the latest DB Deployment Utility from the downloads section of the db-deploy-util Bitbucket repo  [dbdeploy.exe](https://bitbucket.org/Retail_Development/db-deploy-util/downloads/dbdeploy.exe)

### Other tools

7zip (http://www.7-zip.org/download.html)
