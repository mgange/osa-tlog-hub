## Customising the build profile

The build tool uses yaml, a form of markup, to express build actions.

Different users and environments my require different deployment details. These can be configured through a custom build profile.

### Build Profile Precedence

The build tool requires a default profile. This should never be edited by a non-build user of the system (including general development). These settings are project specific, not user specific.

The precedence for retrieving configuration settings is:

1. User build profile
2. Default build profile
3. Error

### How to use a custom build profile

1. Copy the /build/profiles/customProfile.yml.sample file to a new file with a sensible name like ktang.yml or db21.yml
2. Remove any settings that you do not want to customise (the defaults will be used in their place)
3. Edit the copied build profile to suit your requirements
4. Pass in the custom build profile when calling the build or release scripts (see below)

#### Developer

The developer build script is store in /build/tools/build.ps1 and has the following signature.

```
.\build.ps1 -task <sometask> -buildProfile <UserProfile>
```
The buildProfile and task variables are optional in the developer build script.

#### Release

The release script is stored in /build/tools/release.ps1 and has the following signature.

```
.\release.ps1 -buildProfile <ServerProfile>
```
The buildProdile is **not** optional in the release build script.

### About yaml

yaml is a space separated config file. Think of it like a super easy version of xml. The critical aspect of yaml is space indentation. The indention provides the configuration levels.

Consider these two yaml files
**Sample1.yml**
```
build:
  key: val
```

**Sample2.yml**
```
build:
key: val
```

To access the _val_ in _Sample1.yml_, we'd access _build.key_, but in _Sample2.yml_ we'd simply call _key_.

### Default Build Profile

The default build profile for the project can be found
 [here](https://bitbucket.org/Retail_Development/build-tools/src/master/build/profiles/default.yml?at=master&fileviewer=file-view-default)

### Custom Build Profile

Settings that could commonly be changed in your user profile are servers and tool paths.

Here is a snippet for a yaml file that sets the sql deploy server to a named instance

```
db:
  server: .\\SQL2014
```
