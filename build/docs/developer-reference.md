## Getting Started

* Clone this repository
* Download the latest DB Deployment Utility from the downloads section of the db-deploy-util Bitbucket repo  [dbdeploy.exe](https://bitbucket.org/Retail_Development/db-deploy-util/downloads/dbdeploy.exe)
* Put dbdeploy.exe in ./osa-crossmark/build/tools
* If you are cloning from sourcetree, the submoules will initialise automatically
* If you are clonfing from the command line, initialise the submodules
```
cd /path/to/this/repo
git submodule init
```

## Development TL;DR;

To get a deployed project up on your local instance ASAP.

1. Open Powershell
2. cd /path/to/repo/osa-crossmark/build/tools
3. .\build.ps1

## Development - more detail

Build and deploy configurations can be customised. For more information on customising the build profile see (here)[https://bitbucket.org/Retail_Development/build-tools/src/master/build/docs/tooling/custom-build-profile.md]

## Concepts

OSA uses a deployment utility, dbdeploy. This utility can perform a number of tasks such as run a number of sql scripts against a db instance, model a database and verify upgrade scripts to be run.

The utility takes an object (more on this later) that contains all of the scripts required to setup and upgrade a database. In much the same way, we can provide dbdeploy with a set of scripts that downgrades functionality or seeds the system with dev data etc.

The important concepts for this utility are:

* it needs to follow some file/folder layout rules
* we need to use some special codes in the folder names to specify which databases scripts should be executed against
  * The folder structure needs to be v\[Major\].\[Minor\].\[Patch\]\\[seq\]-\[dbkey\]\any\path\to\file
    * e.g.
        * v1.0.0\01-efa\table.sql
        * v1.0.0\02-stg\procs\proc.sql
        * v1.0.0\03-eng\views\view.sql
* scripts are run in order, the sequence number is used to specify the run order (otherwsie alphabetical)
* when we execute the dbdeploy script, we tell the utility which dbkey makes to which database - this allows us to deploy to multiple databases in one upgrade.

Each script will only be executed on a target server once. Once run, they will not be run again. If a script fails it will be run again so consider script re-runability. The best way to account for this is by performing only one operation per script. Special folders 'pre-execute' and 'post-execute' will run during every upgrade.

** It is vital that once scripts are run, they are not edited or renamed.! **

## How to develop an upgrade

DbDeploy upgrades are used as the only deployment mechanism for this system.

1. Plan the version in Jira. The versioning system used is v\[Major\].\[Minor\].\[Patch\]
2. Create a folder /path/to/repo/db/v\[Major\].\[Minor\].\[Patch\]
3. Script the upgrade into files (see Db Scripting Best Practice below) for the version being developed

## How to model the database

It is useful to model the DB to maintain an up-to-date script-per-object model of the database. This enables additional functionality such as git-diff for change tracking.

** This functionality will be added to CI, users should never commit their db-model directory **

* To model the database, the build task is called ModelDb

In Powershell
```
cd /path/to/repo/build/tools
.\build.ps1 -task ModelDb
```

## Best Practice

* Create one object per script. This is vital. If a script fails mid-way through execution, it will be re-run. If the script contains non-rerunnable statements (like adding columns to a table), the upgrade will be impossible to complete.
* Remember you are coding transformations, this means you can code a multi-step transform such as creating a table (t') with re-ordered columns, copying rows from table t to t', dropping t and renaming t' as t. You have full control over the transform code.
* If you need to develop with an existing object (o) you need to ensure you have the latest copy to work with
    * pull you repo to get latest
    * deploy latest to a local instance
    * script the object (o) out of the database
