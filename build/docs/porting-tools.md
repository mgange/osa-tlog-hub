## Configuring the build system for a project

If you are starting a new project or refactoring an existing project, you'll need to reconfigure the build system.

All of the assets needed to operate the automated build tooling are stored in projectroot\\build\\tools. The system is designed to be configuration managed for ssis and dbdeploy  projects. Other project types are not yet supported.

### How to port the build system

*Setup required build tools*

1. Copy the oldprojectroot\\build\\tools directory and all contents to newprojectroot\\build\\tools
2. Create newprojectroot\\build\\profiles directory, and copy oldprojectroot\\build\\profiles\\default.yml and custom.yml.sample to newprojectroot\\build\\profiles\\default.yml to use as a template
3. Delete the newprojectroot\\build\\tools\\PowerYaml directory as we'll be receiving this from a git submodule
4. Add [dbdeploy.exe](https://bitbucket.org/Retail_Development/db-deploy-util/downloads/dbdeploy.exe) to the newprojectroot\\build\\tools directory if not already present

*Setup require submodules*

5. Add the PowerYaml submodule with the following command from git bash / git terminal

```
git submodule add https://github.com/scottmuc/PowerYaml.git newprojectroot/build/tools/PowerYaml
```

*Create your projects*

6. Add your ssis and/or dbdeploy projects under your newprojectroot, for example:
```
> /newprojectroot
> /newprojectroot/build
> /newprojectroot/build/tools
> /newprojectroot/db
> /newprojectroot/etl
```
7. dbdeploy projects need to follow the guidelines set out in the developer-reference documentation
8. dbdeploy projects should contain this [build project](https://bitbucket.org/Retail_Development/db-deploy-util/src/master/docs/dbdeploy-project.dbuproj.sample?at=master&fileviewer=file-view-default) at the db project root. 

*Configure your build profiles*

7. This step is bespoke to your project. The default.yml is heavily commented to help you configure it. This will be further documented in time, but the inline comments should be sufficient to get started.
8. Alter newprojectroot//build//profiles//custom.yml.sample to provide the bare minimum configuration file containing the likely properties that another developer may require. This should not include any project paths or deploy, test teardown action elements, since these should remain common for all developers.
