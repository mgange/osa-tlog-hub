## Getting Started

### Get the release bundle

Take the release bundles supplied by development either from

* TeamCity
* Directly (process tbc)

### Pre-flight checklist

1. Solution targets SQL2014
2. Shut down any jobs
3. Backup the databases

### Prepare the release bundle

1. Unpack the release bundle
2. Open Powershell
3. cd /path/to/releasebundle/build/tools
4. ./release.ps1 -buildProfile <profile.yml>

In step 4, the profile.yml needs to be the release profile configure correctly for the target server. Ask development for profile creations if in doubt.

The release tooling will deploy any additional upgrades not yet run on the target server, and will redeploy all SSIS packages.

### Exceptional Cases

If tech-ops have authorised development to perform 'out-of-process' changes to the production server, a special additional step needs to be undertaken. This step involves marking changes that development apply directly to production/UAT environment as released.

This stems from upgrades being stored in the dbo.SchemaVersions table. If a change is made to a database without the upgrade tool, the upgrade tool needs to be informed of this change. We do this by reverse engineering the change into source control to ensure we have an accurate record of the production change, then running the release in 'mark' mode, which marks any outstanding upgrades as complete without running any scripts against the server.

This isn't yet supported (see CMK-51).
