param([String]$task = "Deploy") 

Write-Host "psake brewing $task"

$p = Split-Path $script:MyInvocation.MyCommand.Path
$b = Join-Path $p "build-lib.ps1"
$m = Join-Path $p "psake.psm1"
#$buildProfile = Join-Path $p "..\profiles\paul.yml"

Remove-Module [p]sake; 
& {Import-Module $m; Invoke-psake $b -task $task; Write-Host $lastexitcode; Remove-Module [p]sake; }