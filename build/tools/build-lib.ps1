properties {
	$dconf = $null
	$uconf = $null
}

task Build -depends BuildDb
task Release -depends DeployDb
task Deploy -depends BuildDb, PackageRelease, DeployDb
task Test -depends BuildDb, TestDb
task TearDown -depends BuildDb, TeardownDb
task default -depends Deploy
task dbOnly -depends BuildDb, DeployDb

task scratch {
	
}

task InitBuild {
	if(!(Test-Path $(Get-ToolPathDbDeploy)))
	{
		Write-Error "Cannot find dbdeploy.exe at $dbdeploy.`nDownload dbdeploy.exe from downloads in db-deploy-utility Bitbucket repo."
	}
	Delete-Directory $(Get-LogsPath)
	Create-Directory $(Get-LogsPath)
}

task BuildDb -depends InitBuild {
	$(Resolve-ProjectPaths "db" "build") | Build-DbProjects
}

task DeployDb -depends InitBuild {
	$(Resolve-ProjectPaths "db" "deploy") | Deploy-DbProjects
}

task TestDb -depends InitBuild {
	$(Resolve-ProjectPaths "db" "test") | Deploy-DbProjects
}

task TeardownDb -depends InitBuild {
	$(Resolve-ProjectPaths "db" "teardown") | Deploy-DbProjects
}

task BuildSsis -depends InitBuild {
	$(Resolve-ProjectPaths "ssis" "build") | Build-SsisProjects
}

task DeploySsis -depends InitBuild {
	$(Resolve-ProjectPaths "ssis" "deploy") | Deploy-SsisProjects
}

task TestSsis {
	Write-Warning "Test SSIS not implemented yet"
}

task TeardownSsis {
	Write-Warning "Teardown SSIS not implemented yet"
}

task InitModel {
	
	Delete-Directory $(Get-ConfigValue "model.path")
}

task ModelDb -depends InitModel {
	$(Resolve-ProjectPaths "db" "deploy") | Model-DbProjects
}

task PackageRelease {
	Package-Release
}

<# takes a custom powershell object #>
function Build-DbProjects {
	param(
        [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true)]
        [PSObject[]]$pipe
    )
	process {
		$pipe | % {
			$props = @{}
			Run-MSBuild  -project $($_.Path) `
							-target "clean;build" `
							-properties $props `
							-log $(Get-LogFile "build_db_$($_.Name).log")
			Write-Output $_
		}
	}
}

<# takes a custom powershell object #>
function Deploy-DbProjects {
	param(
        [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true)]
        [PSObject[]]$pipe
    )
	process {
		$pipe | % {
			Run-DbDeploy -verb "Upgrade" `
							-sf $(Join-Path $($_.Directory) "\bin\$($_.Name).dll") `
							-tsn $(Get-ConfigValue "db.server") `
							-cto $(Get-ConfigValue "db.timeoutsecs") ` `
							-tdns $(Get-ConfigValue "db.dbkeys") `
							-sqlCmdVars $(Get-ConfigValue "db.sqlcmdvars") `
							-log $(Get-LogFile "deploy_db_$($_.Name).log")
			Write-Output $_
		}
	}
}

function Model-DbProjects {
	param(
        [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true)]
        [PSObject[]]$pipe
    )
	process {
		$pipe | % {
			Run-DbDeploy -verb "Model" `
							-op $(Get-ConfigValue "model.path") `
							-sf $(Join-Path $($_.Directory) "\bin\$($_.Name).dll") `
							-tsn $(Get-ConfigValue "db.server") `
							-cto $(Get-ConfigValue "db.timeoutsecs") ` `
							-tdns $(Get-ConfigValue "model.dbkeys") `
							-sqlCmdVars $(Get-ConfigValue "db.sqlcmdvars") `
							-log $(Get-LogFile "deploy_db_$($_.Name).log")
			Write-Output $_
		}
	}
}

function Build-SsisProjects {
	param(
        [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true)]
        [PSObject[]]$pipe
    )
	begin {
		$projectModel = $(Get-ConfigValue "ssis.deploymentmodel") -ieq "project"
	}
	process {
		$pipe | % { if($projectModel) { Build-SsisProjectProjects $_ } }
	}
}

function Build-SsisPackageProjects {
	param(
        [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true)]
        [PSObject[]]$pipe
    )
	begin {
		Write-Information "We don't need to build ssis package projects."
	}
	process {
		$pipe | % { Write-Output $_	}
	}
}

<# Build a project style ssis project #>
function Build-SsisProjectProjects {
	param(
        [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true)]
        [PSObject[]]$pipe
    )
	process {
		$pipe | % {
			Run-DevEnvBuild 	-project $($_.Path) `
			 					-target "build" `
								-configuration $(Get-ConfigValue "ssis.buildconfig") `
								-log $(Get-LogFile "build_ssis_$($_.Name).log")
			Write-Output $_
		}
	}
}

function Deploy-SsisProjects {
	param(
        [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true)]
        [PSObject[]]$pipe
    )
	begin {
		$projectModel = $(Get-ConfigValue "ssis.deploymentmodel") -ieq "project"
	}
	process {
		# choose which function to call based on some config value (not yet built)
		$pipe | % {	
			if($projectModel) { Deploy-SsisProjectProjects $_ }
			else { Deploy-SsisPackageProjects $_ }
		}
	}
}

<# takes a custom powershell object #>
function Deploy-SsisPackageProjects {
	param(
        [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true)]
        [PSObject[]]$pipe
    )
	process {
		$pipe | % {
			$folder = Get-ConfigValue "ssis.deploypaths.$($_.Label)"
		    if(!$folder) { $folder = $($_.Name) }
			
			Run-DeploySsisPackages  -srcDir $($_.Directory) `
									-ssisServer $(Get-ConfigValue "ssis.server") `
									-ssisFolder $folder `
									-log $(Get-LogFile "deploy_ssis_$($_.Name).log")
			Write-Output $_
		}
	}
}

<# takes a custom powershell object #>
function Deploy-SsisProjectProjects
{
	param(
        [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true)]
        [PSObject[]]$pipe
    )
	process { 
		$pipe | % {
			$buildCfg = Get-ConfigValue "ssis.buildconfig"
			$sp = $(Join-Path $($_.Directory) "\bin\$buildCfg\$($_.Name).ispac");
			$folder = Get-ConfigValue "ssis.deploypaths.$($_.Label)"
		    if(!$folder) { $folder = $($_.Name) }
	
			Run-DeploySsisProject 	-ssisServer $(Get-ConfigValue "db.server") `
									-ssisFolder $folder `
									-ssisProject $($_.Name) `
									-isPac $sp `
									-log $(Get-LogFile "deploy_ssis_$($_.Name).log")
			Write-Output $_
		}
	}
}

function Package-Release {
	$relPath = $(Get-ConfigValue "packaging.path")
	$outPath = $(Join-Path $(Get-ProjectRootPath) $relPath)
	# this will fail quietly if it already exists
	Create-Directory $outPath
	$outPath = $((Resolve-Path $outPath).Path)
	if(!$relPath -or $outPath -ieq $((Get-ProjectRootPath).Path)) {
		throw "packaging.path is not correctly configured."
	}
	Delete-Directory $outPath
	Create-Directory $outPath
	
	$props = @{"SourceRoot"=$(Get-ProjectRootPath);
			   "Destination"=$outPath;}
	$log = $(Get-LogFile "package.log")

	Run-MSBuild -project $(Get-PackagerHelperPath) `
				-target "CopyFiles" `
				-properties $props `
				-log $log
					
	if([bool]$(Get-ConfigValue "packaging.zip") -ieq $true) {
		Run-ZipDirectory  -src $outPath `
						  -dst $($outPath+".zip") `
					  	  -log $log 
	}
}

function Get-LogFile([string]$name) {
	$log = Join-Path $(Get-LogsPath) $name
	 " -- LOG START --" + $(Get-Date -UFormat "%Y / %m / %d / %A / %Z") | Out-File -Append -Encoding UTF8 $log
	$log
}

function Get-BuildToolPath { $(Resolve-Path $(Split-Path $script:MyInvocation.MyCommand.Path)) }
function Get-BuildProfilesPath { Resolve-Path $(Join-Path $(Get-BuildToolPath) "..\profiles") } 
function Get-ProjectRootPath { Resolve-Path $(Join-Path $(Get-BuildToolPath) "..\..\") } 
function Get-PackagerHelperPath { Resolve-Path $(Join-Path $(Get-BuildToolPath) "packager.proj") } 
function Get-IsPacDeployHelperPath { Resolve-Path $(Join-Path $(Get-BuildToolPath) "deploy-ssis.sql") } 
function Get-LogsPath { Join-Path $(Get-BuildToolPath) "..\logs" } 
function Get-ToolPathDevEnv { Resolve-Path $(Get-ConfigValue "toolpaths.devenv") }
function Get-ToolPathSqlPackager { Resolve-Path $(Get-ConfigValue "toolpaths.sqlpkg") }
function Get-ToolPathSqlCmd { Resolve-Path $(Get-ConfigValue "toolpaths.sqlcmd") }
function Get-ToolPathDtUtil { Resolve-Path $(Get-ConfigValue "toolpaths.dtutil") }
function Get-ToolPathMsTest { Resolve-Path $(Get-ConfigValue "toolpaths.mstest") }
function Get-ToolPathZip { Resolve-Path $(Get-ConfigValue "toolpaths.zip") }
function Get-ToolPathDbDeploy { 
    $path = Join-Path $(Get-ProjectRootPath) $(Get-ConfigValue "toolpaths.dbdeploy")
    if(!(Test-Path $path)) {
        $path = Get-ConfigValue "toolpaths.dbdeploy"
    }
    Resolve-Path $path
}

<# needs to be converted to pipeline function #>
function Resolve-ProjectPaths($projType, $action) {
	$keys = @(Resolve-ProjectKeys $projType $action)
	# Iterate keys, pull the path
	# We cannot use get-item here because in a deployment scenario we won't have access to the .proj file
	$keys | ? { $_ -ne $null -and $_ } | % {
		$cfgPath = "$projType.paths.$_"
		$path = $(Join-Path $(Get-ProjectRootPath) $(Get-ConfigValue $cfgPath))
        #$projects = ,
		write-output $(New-Object -TypeName psobject -Property @{
                            Label = $_
							CfgPath = $cfgPath
							Path = $path
							Name = $(Get-FilenameWithoutExtension $path)
							Directory = $(Get-DirectoryName $path)
                        })
	}
	#$projects
}

function Resolve-ProjectKeys($projType, $action)
{
	#$keys = @()
	# If building we want all of the projects
	# else we just want the projects for the action specified
	if($action -ieq "build") {
		$keys = @($(Get-ConfigValue "$projType.paths" $false).Keys)
	}
	else {
		$keys = @($(Get-ConfigValue "$action.$projType" $false))
	}
	$keys
}

function Get-DefaultBuildProfilePath {
	$bp = Join-Path $(Get-BuildProfilesPath) "default.yml"
	if(!(Test-Path $bp)) {
		throw "Default build profile cannot be found at $bp. Please create a default build profile for all settings."
	}
	$bp
}

<#
	takes the user supplied param $buildProfile and checks
	to see if it is absolute path, a relative path or in the build profiles folder
#>
function Get-UserBuildProfilePath
{
	$bp = $null
	if($buildProfile) {
		$found = $false
		$lookIn = @()
		$lookIn += $(Join-Path $(Get-BuildProfilesPath) $buildProfile)
		$lookIn += $buildProfile
		
		$i = 0
		do {
			$p = $lookIn[$i]
			if((Test-Path -Path $p -PathType Leaf)) {
				$bp = $p
				$found = $true
			}
			$i ++
		} until ($found -or $i -eq $($lookIn.Length))
		if($bp -eq $null) {
			throw "User build profile cannot be found in the following locations $lookin"
		}
	}
	$bp
}

function Get-ConfigValue($yamlPath, $throwOnMissing = $true)
{
	if(!$dconf) {
		# Load Default Config
		$dconf = Get-Yaml -FromFile $(Get-DefaultBuildProfilePath)
	}
	if(!$uconf)	{
		# Load User Config If Exists
		$uconfPath = Get-UserBuildProfilePath
		if($uconfPath)
		{ 
			$uconf = Get-Yaml -FromFile $uconfPath
		}
	}
	if($uconf) {
		$v = Get-FromYaml $uconf $yamlPath
	}
	if(!$v -and $dconf) {
		$v = Get-FromYaml $dconf $yamlPath
	}
	if((!$v -or $v -eq $null) -and $throwOnMissing) {
		throw "Unable to find configuration value at $yamlPath"
	}
	$v
}

function Get-FromYaml($yaml, $yamlPath) {
	if(!$yamlPath) {
		$v = $yaml	
	}
	else {
		$parts = $yamlPath.split(".")
		if($parts.count -eq 0) {
			$v = $yaml.$yamlPath
		}
		else {
			$v = $yaml
			$parts | % {
				$v = $v.$_
			}
		}
	}
	$v
}

################################################################################################
# Common functionality below this point
# Nothing specific to the build tasks
# 
################################################################################################

function Delete-File($file) {
    if($file) { remove-item $file -force -ErrorAction SilentlyContinue | out-null } 
}

function Delete-Directory($directory) {
    if($directory) { remove-item $directory -force -Recurse -ErrorAction SilentlyContinue | out-null } 
}

function Create-Directory($directory) {
  mkdir $directory  -ErrorAction SilentlyContinue  | out-null
}

function Get-DirectoryName([string]$fileName) {
	Split-Path -Path $fileName -Parent
}

function Resolve-PathAndTrim($path) {
	@(Resolve-Path $path) | % { Write-Output $($_.TrimEnd('\')) }
}

function Get-FilenameWithoutExtension([string]$fileName) {
	$fn = Split-Path -Path $fileName -Leaf
	$fn.Substring(0, $fn.LastIndexOf("."))
}

function Get-EnvironmentVariable([string]$varName) {
	(Get-Item env:$varName).Value
}

# Convert kvp to switch array
function Create-SwitchArray([hashtable]$map, [string]$switchPfx, [string]$switchDelim, [bool]$arrStyle=$true) {
	$switches = @()
	$map.Keys | % {
          	$key = $_
           	$val = $map[$_]
			if(!$arrStyle) { 
				$switches += $switchPfx+$key+$switchDelim+$val
			}
			else {
	           	$switches += $switchPfx
				$switches += $key+$switchDelim+$val
			}
     }
	 $switches
}

function Quote-String([string]$str) {
	"`"$str`""
}

function Run-DevEnvBuild {
  [CmdletBinding()]param(  [string]$project, [string]$target, [string]$configuration, [string]$log )

	$exe = $(Get-ToolPathDevEnv)
	$cmdArgs = @()
	$cmdArgs += Quote-String $project
	$cmdArgs += "/$target"
	$cmdArgs += Quote-String "$configuration"

	# Execute
	exec { Run-LoggedCommand $exe $cmdArgs $log }
}

function Run-MsBuild {
  [CmdletBinding()]param( [string]$project, [string]$target, [HashTable]$properties, [string]$log, [string]$verbosity = "n" )

	$cmdArgs = @()
	$cmdArgs += Quote-String $project
	$cmdArgs += "/t:$target"
	$cmdArgs += "/v:$verbosity"
	$cmdArgs += Create-SwitchArray $properties "/p:" "=" $false

	# Execute
	exec { Run-LoggedCommand "msbuild" $cmdArgs $log }
}

function Run-MsTest {
  [CmdletBinding()]param( [string]$name, [string]$proj, [string]$trx, [string]$log )
    
	Delete-File $trx
	
	$exe = $(Get-ToolPathMsTest)
	
	$switches = @()
	$switches += "/testcontainer:$proj"
	$switches += "/resultsfile:$trx"
	
    exec { Run-LoggedCommand $exe $switches $log }
}

function Run-SqlCmd {
	[CmdletBinding()]param( [string]$tsn, [string]$tdn, [string]$inputFile, [HashTable]$sqlCmdVars, [string]$log )
	# we are expecting the sqlcmdvars to be quoted appropriately

	$vars = @()
	$vars += Create-SwitchArray $sqlCmdVars "" "=" $false
	# setup logs
	$logContent = [string]$null
	"Invoking ps-sqlcmd command tsn: $tsn, tdn: $tdn, i: $i, variables: $vars" | Tee-Object -Variable logContent
	$logContent | out-file -Append -encoding UTF8 $log
	# Invoke command
	(Invoke-Sqlcmd -Server $tsn -Database $tdn -InputFile $inputFile -AbortOnError -Variable $vars -Verbose) 4>&1 | Tee-Object -Variable logContent
	$logContent | out-file -Append -encoding UTF8 $log
}

function Run-SqlPackage {
  [CmdletBinding()]param( [string]$a = $null, [string]$tsn = $null, [string]$tdn = $null, [string]$sf = $null,[hashtable]$properties, [hashtable]$sqlCmdVars, [string]$log = $null )
	$exe = $(Get-ToolPathSqlPackager)
    $switches = @()
    #add well known switches like tsn/tdn
	$switches += "/a:" + $a
	$switches += "/sf:" + $sf
	$switches += "/tsn:" + $tsn
	$switches += "/tdn:" + $tdn
	#add properties
	$switches += Create-SwitchArray $properties "/p:" "=" $true
	#add sqlcmdvars
	$switches += Create-SwitchArray $sqlCmdVars "/v:" "=" $true
	
	# Execute 
	exec { Run-LoggedCommand $exe $switches $log }
}

function Run-DbDeploy {
  [CmdletBinding()]param( 
  	[string]$verb, [string]$sf, [string]$tsn, [int]$cto, [hashtable]$tdns, [hashtable]$sqlCmdVars, [string]$log, [string]$op=$null )
	
	$exe = $(Get-ToolPathDbDeploy)
	# Set switches
	$switches = @()
	$switches += $verb
	$switches += "-sf"
	$switches += Quote-String $sf
	$switches += "-tsn"
	$switches += Quote-String $tsn
	$switches += "-to"
	$switches += $cto
	$switches += Create-SwitchArray $tdns "-tdn" "=" $true
	$switches += Create-SwitchArray $sqlCmdVars "-v" "=" $true
	
	if (![string]::IsNullOrEmpty($op)) {
		$switches += "-op"
		$switches += Quote-String $op
	}

	exec { Run-LoggedCommand $exe $switches $log }
}

function Run-DeploySsisProject {
	[CmdletBinding()]param( [string]$ssisServer, [string]$ssisFolder, [string]$ssisProject, [string]$isPac, [string]$log )

	$sqlCmdVars = @{ "foldername"="`'$ssisFolder`'"; `
					 "projectname"="`'$ssisProject`'"; `
					 "ispacpath"="`'$isPac`'"; }

	Run-SqlCmd $ssisServer "SSISDB" $(Get-IsPacDeployHelperPath) $sqlCmdVars $log
}

function Run-DeploySsisPackages {
	[CmdletBinding()] param( [string]$srcDir, [string]$ssisServer, [string]$ssisFolder, [string]$log )
	
	$exe = $(Get-ToolPathDtUtil)
	Run-CreateSsisPackageFolders $ssisFolder $ssisServer $log
	gci $srcDir | ? {$_.extension -ieq ".dtsx"} | % {
		Write-Host "Deploying Ssis Package $_ to $ssisServer at $ssisFolder"
		$cmdArgs = @()
		$cmdArgs += "/FILE"
		$cmdArgs += "`"$($_.FullName)`""
		$cmdArgs += "/COPY"
		$cmdArgs += "SQL;`"$ssisFolder\$($_.BaseName)`""
		$cmdArgs += "/DESTSERVER"
		$cmdArgs += "$ssisServer"
		$cmdArgs += "/QUIET"
		exec { Run-LoggedCommand $exe $cmdArgs $log }
	}
}

function Run-CreateSsisPackageFolders( [string]$ssisFolder, [string]$server, [string]$log ) {
	# path accumulator
	# special root starting position
	$exe = $(Get-ToolPathDtUtil)
	$pathHead = "\\"
	$ssisFolder.Split("\", [System.StringSplitOptions]::RemoveEmptyEntries) | % {
		# proposed path head for checking existience
		$nextPathHead = "$pathHead\$_"
		# special case for root folder, thanks MS you douche
		if($pathHead -ieq "\\") { $nextPathHead = "\\$_" }
		Write-Host "Checking Ssis Folder $pathHead"
		$cmdArgs = "/FE SQL;$(Quote-String $nextPathHead) /SOURCES $server /QUIET"
		Run-LoggedCommand $exe $cmdArgs $log
		if ($LASTEXITCODE -eq 1) { 
			Write-Host "Creating Ssis Folder $nextPathHead"
			$cmdArgs = "/FC SQL;$(Quote-String $pathHead);$(Quote-String $_) /SOURCES $server /QUIET"	
			Run-LoggedCommand $exe $cmdArgs $log
		}
		$pathHead = $nextPathHead
	}
}

function Run-ZipDirectory($src, $dst, $log) {
    Delete-File $file
	$exe = $(Get-ToolPathZip)
	$switches = @("a", "-tzip", $dst, $src)
	exec { Run-LoggedCommand $exe $switches $log }
}

function Run-LoggedCommand ($cmd, $cmdArgs, $log)
{
	$logContent = [string]$null
    "Invoking external command $cmd $cmdArgs" | Tee-Object -Variable logContent
	$logContent | Out-File -Append -Encoding UTF8 $log

	# using Tee-Object we can't control the encoding of the output, this makes the log un-readable
    & $cmd $cmdArgs 2>&1 | Tee-Object -Variable logContent
    $logContent | Out-File -Append -Encoding UTF8 $log
}

function Run-BatchCommand ($cmd, $cmdArgs, $log)
{
	$logContent = [string]$null
    "Invoking external batch command $cmd $cmdArgs" | Tee-Object -Variable logContent
	$logContent | Out-File -Append -Encoding UTF8 $log

	# using Tee-Object we can't control the encoding of the output, this makes the log un-readable
    cmd /c "`"$cmd`" $cmdArgs 2>&1" | Tee-Object -Variable logContent
    $logContent | Out-File -Append -Encoding UTF8 $log
}