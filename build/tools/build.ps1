param([String]$task, [String]$buildProfile)

Write-Host "------------------------";
Write-Host "psake brewing entrypoint";
Write-Host "------------------------";
Write-Host "Information";
Write-Host "------------------------";
Write-Host "Invoke the ps build entrypoint from the command line using...";
Write-Host "     powershell .\build.ps1"
Write-Host "Invoke the ps build entrypoint from the command line whilst specifying a task and profile using...";
Write-Host "     powershell .\build.ps1 -task `"<task>`" -buildProfile `"<profile>`"";
Write-Host "     e.g. powershell .\build.ps1 -task `"Package`" -buildProfile `"..\profiles\myprofile.yaml`"";
Write-Host ""
Write-Host "Available tasks:";
Write-Host "     Deploy";
Write-Host "     Test";
Write-Host "     Teardown";
Write-Host "";
Write-Host "The default task in Deploy";
Write-Host "------------------------";
Write-Host "Brewing psake";
Write-Host "";
Write-Host "task:          " $(if($task) {"$task"} else {"default"});
Write-Host "build profile: " $(if("$buildProfile") {$buildProfile} else {"default"});
Write-Host "------------------------";

Remove-Module [p]sake; 
if($task -and $buildProfile) {
	& {Import-Module .\psake.psm1; Invoke-psake .\build-lib.ps1 -task $task -parameters @{"buildProfile"="$buildProfile"}; Write-Host $lastexitcode; Remove-Module [p]sake; }
} elseif($task) {
	& {Import-Module .\psake.psm1; Invoke-psake .\build-lib.ps1 -task $task; Write-Host $lastexitcode; Remove-Module [p]sake; }
} elseif($buildProfile) {
	& {Import-Module .\psake.psm1; Invoke-psake .\build-lib.ps1 -parameters @{"buildProfile"="$buildProfile"}; Write-Host $lastexitcode; Remove-Module [p]sake; }
} else {
	& {Import-Module .\psake.psm1; Invoke-psake .\build-lib.ps1; Write-Host $lastexitcode; Remove-Module [p]sake; }
}
