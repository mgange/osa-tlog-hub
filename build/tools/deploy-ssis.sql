DECLARE @operation_id INT
DECLARE @folderName NVARCHAR(128)
RAISERROR ('Specify SSIS deployment folder name', 16, 1)
PRINT '-- Input Vars --'
PRINT 'FN: ' + $(foldername)
PRINT 'PN: ' + $(projectname)
PRINT 'ISP: ' + $(ispacpath)

IF $(foldername) = ''
BEGIN
  RAISERROR ('Specify SSIS deployment folder name', 16, 1)
  RETURN
END

IF $(projectname) = ''
BEGIN
  RAISERROR ('Specify SSIS deployment project name', 16, 1)
  RETURN
END

IF NOT EXISTS (SELECT 0 FROM catalog.folders where name = $(foldername))
BEGIN
	EXEC [catalog].[create_folder] @folder_name = $(foldername)
END

DECLARE @ProjectBinary as VARBINARY(max)
DECLARE @ParmDefinition NVARCHAR(500);
DECLARE  @sql NVARCHAR(MAX)
SELECT @sql = N'SELECT @bin = BulkColumn FROM OPENROWSET(BULK '$(ispacpath)', SINGLE_BLOB) as BinaryData';
SET @ParmDefinition = N'@bin VARBINARY(max) OUTPUT';

EXEC sp_executesql @sql, @ParmDefinition, @bin=@ProjectBinary OUTPUT;

EXEC catalog.deploy_project @folder_name = $(foldername), @project_name = $(projectname), @Project_Stream = @ProjectBinary, @operation_id = @operation_id out
