param([String]$buildProfile)

Write-Host "------------------------";
Write-Host "psake brewing entrypoint";
Write-Host "------------------------";
Write-Host "Information";
Write-Host "------------------------";
Write-Host "Invoke the ps build entrypoint from the command line using...";
Write-Host "     powershell .\release.ps1"
Write-Host "Invoke the ps build entrypoint from the command line whilst specifying a task and profile using...";
Write-Host "     powershell .\release.ps1 -buildProfile `"<profile>`"";
Write-Host "     e.g. powershell .\release.ps1 -buildProfile `"..\profiles\serverprofile.yaml`"";
Write-Host ""
Write-Host "------------------------";
Write-Host "Brewing psake";
Write-Host "";
Write-Host "build profile: $buildProfile";
Write-Host "------------------------";

Remove-Module [p]sake; 
if($buildProfile) {
	& {Import-Module .\psake.psm1; Invoke-psake .\build-lib.ps1 -task Release -parameters @{"buildProfile"="$buildProfile"}; Write-Host $lastexitcode; Remove-Module [p]sake; }
} else {
	Write-Error "You must supply a build profile for the deployment server. See development for further details."
}
