USE master;
GO
DECLARE @sql NVARCHAR(MAX)
if db_id('$(OSCA_ETL_Framework_Admin)') is not null
begin
  SET @sql = 'ALTER DATABASE [$(OSCA_ETL_Framework_Admin)] SET SINGLE_USER WITH ROLLBACK IMMEDIATE'
  EXEC sp_executesql @sql
  SET @sql = 'DROP DATABASE [$(OSCA_ETL_Framework_Admin)]'
  EXEC sp_executesql @sql
  SET @sql = null
end

if db_id('$(OSCA_Stage)') is not null
begin
  SET @sql = 'ALTER DATABASE [$(OSCA_Stage)] SET SINGLE_USER WITH ROLLBACK IMMEDIATE'
  EXEC sp_executesql @sql
  SET @sql = 'DROP DATABASE [$(OSCA_Stage)]'
  EXEC sp_executesql @sql
  SET @sql = null
end

if db_id('$(OSCA_Engine)') is not null
begin
  SET @sql = 'ALTER DATABASE [$(OSCA_Engine)] SET SINGLE_USER WITH ROLLBACK IMMEDIATE'
  EXEC sp_executesql @sql
  SET @sql = 'DROP DATABASE [$(OSCA_Engine)]'
  EXEC sp_executesql @sql
  SET @sql = null
end

if db_id('$(OSCA_ItemStoreBucketDay)') is not null
begin
  SET @sql = 'ALTER DATABASE [$(OSCA_ItemStoreBucketDay)] SET SINGLE_USER WITH ROLLBACK IMMEDIATE'
  EXEC sp_executesql @sql
  SET @sql = 'DROP DATABASE [$(OSCA_ItemStoreBucketDay)]'
  EXEC sp_executesql @sql
  SET @sql = null
end

if db_id('$(OSCA_Reporting)') is not null
begin
  SET @sql = 'ALTER DATABASE [$(OSCA_Reporting)] SET SINGLE_USER WITH ROLLBACK IMMEDIATE'
  EXEC sp_executesql @sql
  SET @sql = 'DROP DATABASE [$(OSCA_Reporting)]'
  EXEC sp_executesql @sql
  SET @sql = null
end
GO
