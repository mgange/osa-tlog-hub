
function Print-Error {
	Write-Host "Line $($_.InvocationInfo.ScriptLineNumber): $($_.InvocationInfo.Line) in $($_.InvocationInfo.ScriptName)" -ForegroundColor Gray
	Write-host "Exception Type: $($_.Exception.GetType().FullName)" -ForegroundColor Red
	Write-host "Exception Message: $($_.Exception.Message)" -ForegroundColor Red
}




function Get-Config-Property {
	param($sqlConnection, [string] $dbName, [string] $configName)
	$val = $sqlConnection.QueryScalar("SELECT ConfigValue FROM metadata.CoreConfig WHERE DBName='$dbName' and ConfigName='$configName'")
	return $val
}

# Add days to period_key which data type is integer, but content is datetime
function Add-DateID {
	param([int] $dateID, [int] $days)
	$val = [int][DateTime]::ParseExact($dateID,"yyyyMMdd",[System.Globalization.CultureInfo]::InvariantCulture).AddDays($days).ToString("yyyyMMdd")
	return $val
}

function Add-Column {
	param($sqlConnection, $dbName, $columns)   

	$tableNames = @()
	# every third record is a table name 
	for($i=0; $i -le $columns.Count; $i++) {
		if(($i+1) % 3 -eq 0) {
			$tableNames += $columns[$i]
		}
	}

	$allTableNames = ($tableNames | % { $_.ToUpper() } | Select -Unique ) -join "', '"

	$sql  = "SELECT o.name AS table_name, c.name AS column_name FROM sys.columns c INNER JOIN sys.objects o ON c.object_id = o.object_id WHERE o.name IN ('$allTableNames') AND o.type = 'U'"
	$cols = $sqlConnection.QueryTable($sql)

	$addColumnSql= ""
	for($i=0;$i -lt ($columns.Count)/3;$i++) {
		($columnName, $dataType, $tableName) = $columns[(3*$i)..(2+3*$i)]
	    if ($cols.SELECT("table_name = '$tableName'").Length -gt 0 -and $cols.select("table_name = '$tableName' and column_name = '$columnName'").Length -eq 0) {

				$addColumnSql += "ALTER TABLE $tableName ADD COLUMN `"$columnName`" $dataType;"
		}
	}
	if ($addColumnSql) {
	    $sqlConnection.Execute($addColumnSql)
		write-host "New Columns added successfully in $dbName"
    } else {
        write-host "No new columns to add in $dbName"
    }
	return $null
}

if($global:ticks -eq $null) {
	$global:ticks = @{}
}

function Start-Tick {
	param($name)
	$global:ticks[$name] = [System.Diagnostics.Stopwatch]::StartNew()
}



function End-Tick{
	param($name)
	$diff = $global:ticks[$name].Elapsed
	$global:ticks[$name].Stop()
	[void] $global:ticks.Remove($name)
	return $diff
}

function Show-Tick{
	param($name)
	$diff = $global:ticks[$name].Elapsed
	return $diff
}

function Assert-ExitCode {
    param($block,$code=0)
    $global:LastExitCode = $code 
    & $block
    if($global:LastExitCode -ne $code) { Throw }
}


function enableSQLServerOption {
	PARAM($sqlConn, $option, $value)
	$sql = "sp_configure '$option', $value; reconfigure with override;"
	$sqlConn.Execute($sql)
}

function Execute-OnSharedNode {
    Param($sharedNode, $cmd)
    try {
        $path = $sharedNode.basePath
        $dbName = $sharedNode.dbName
        $ssh = New-Object Renci.SshNet.SshClient($sharedNode.server, $sharedNode.username, (Decrypt $sharedNode.password))
        $ssh.KeepAliveInterval = New-TimeSpan -Seconds 300
        $maxTry = 5
        for($i = 0; $i -lt $maxTry;  $i++ ){
            try{
                $ssh.Connect()
                break
            }
            catch{
                if($i -eq $maxTry-1){
                    throw
                }
                Start-Sleep -s 1
            }
        }

        $cmd = "cd $path/$dbName; $cmd"
        Log-Debug "$cmd"
        $result = $ssh.RunCommand($cmd)
		if($result.ExitStatus -ne 0) {
		  throw "Execute $cmd failed with $($result.Error)" 
		}
		return $result.Result
    } catch {
        throw 
    } finally {
        if($ssh.IsConnected) {
            $ssh.Disconnect()
            $ssh.Dispose()
        }        
    }
}