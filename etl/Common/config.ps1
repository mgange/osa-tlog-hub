﻿. "$PSScriptRoot\connect.ps1"
function Get-Config {
<#
.SYNOPSIS
Loads configuration from sql server

.DESCRIPTION
1) Fetches all config properties for the given db
2) Returns a hash map with the result

.Example

$config = Get-Config -dbName "DATABASE_NAME" -connection $dbConn

# Returns the value for dw.server.name, if no such property it returns localhost
$config.GetProperty('dw.server.name','localhost')

# Some properties should not be cached, therefor they will be looked up every time GetProperty is called.
$config.GetProperty('etl.transfernum', '-1', $dbName)

#>
param([string]$sqlServerName =".\sql2014", [string]$sqlDBName="HUB_POC", [string]$dbName="HUB_POC", $connection=$null)
$ErrorActionPreference = "Stop"
	try {
		if($connection) {
			$localConnection = $connection
		} else {
			$localConnection = Create-SqlConnection $sqlServerName $sqlDBName
		}
		$configMap = @{'internal.dbName'=$dbName}
		$res = $localConnection.Query("select ConfigName AS name, [metadata].[fn_GetCoreConfig](ConfigName) value from [metadata].[CoreConfig] WHERE dbName='$dbName'")
		foreach($row in $res) {
			[void] $configMap.Add($row[0], $row[1])
		}
		$configMap | Add-Member GetProperty -MemberType ScriptMethod -Value {
			param([string] $name, [string] $defaultValue, $connection)
			if($connection) {
				$dbName = $this['internal.dbName'] 
 				$res    = $connection.Query("select ConfigName AS name, [metadata].[fn_GetCoreConfig](ConfigName) value from [metadata].[CoreConfig] WHERE dbName='$dbName' and ConfigName = '$name'")
				foreach($row in $res) {
					$this[$row[0]] = $row[1]
				}				
			}
			if($this.ContainsKey($name)) {
				$val = $this.Get_Item($name)
				if ([string]::IsNullOrEmpty($val)) {$val = $defaultValue}
				return $val
			} else {
				return $defaultValue
			}
		}
		$configMap | Add-Member GetBooleanProperty -MemberType ScriptMethod -Value {
			param([string] $name, [string] $defaultValue, $connection)
			return $this.GetProperty($name, $defaultValue, $connection) -eq "true"			
		}
		$configMap | Add-Member GetEncryptedProperty -MemberType ScriptMethod -Value {
			param([string] $name, [string] $defaultValue, $connection)
			$value =  $this.GetProperty($name, $null, $connection)
			if(-Not $value) {
				return $defaultValue
			} else {
				. "$PSScriptRoot\Crypto.ps1"
				return Decrypt $value 
			}
		}
	} finally {
		if ($connection -eq $null -And $localConnection -ne $null) {
			[void] $localConnection.Close()
		}
	}
	return $configMap
}


function Get-Config-Synced {
param([string]$sqlServerName =".\sql2014", [string]$sqlDBName="HUB_POC", [string]$dbName="HUB_POC", $connection=$null)
$ErrorActionPreference = "Stop"
	try {
		if($connection) {
			$localConnection = $connection
		} else {
			$localConnection = Create-SqlConnection $sqlServerName $sqlDBName
		}
		$configMap = @{'internal.dbName'=$dbName}
		$res = $localConnection.Query("select ConfigName AS name, [metadata].[fn_GetCoreConfig](ConfigName) value from [metadata].[CoreConfig] WHERE dbName='$dbName'")
		foreach($row in $res) {
			[void] $configMap.Add($row[0], $row[1])
		}
		$configMap | Add-Member GetProperty -MemberType ScriptMethod -Value {
			param([string] $name, [string] $defaultValue, $connection)
			if($connection) {
				$dbName = $this['internal.dbName'] 
 				$res    = $connection.Query("select ConfigName AS name, [metadata].[fn_GetCoreConfig](ConfigName) value from [metadata].[CoreConfig] WHERE dbName='$dbName' and ConfigName = '$name'")
				foreach($row in $res) {
					$this[$row[0]] = $row[1]
				}				
			}
			if($this.ContainsKey($name)) {
				$val = $this.Get_Item($name)
				if ([string]::IsNullOrEmpty($val)) {$val = $defaultValue}
				return $val
			} else {
				return $defaultValue
			}
		}
		$configMap | Add-Member GetBooleanProperty -MemberType ScriptMethod -Value {
			param([string] $name, [string] $defaultValue, $connection)
			return $this.GetProperty($name, $defaultValue, $connection) -eq "true"			
		}
		$configMap | Add-Member GetEncryptedProperty -MemberType ScriptMethod -Value {
			param([string] $name, [string] $defaultValue, $connection)
			$value =  $this.GetProperty($name, $null, $connection)
			if(-Not $value) {
				return $defaultValue
			} else {
				. "$PSScriptRoot\Crypto.ps1"
				return Decrypt $value 
			}
		}
	} finally {
		if ($connection -eq $null -And $localConnection -ne $null) {
			[void] $localConnection.Close()
		}
	}
	return $configMap
}