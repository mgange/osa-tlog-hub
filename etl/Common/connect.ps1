function Create-SqlConnection {
	PARAM($server, $dbName, $config=$null, $user=$null,$pwd=$null,$CommandTimeout = 10800)
	# default command timeout is 3 hours
	if($config) {
		if(-Not $server) {
			$server = $config.serverName
		}
		if(-Not $dbName) {
			$dbName = $config.dbName
		}
	}

	$sqlConnection = New-Object System.Data.SqlClient.SqlConnection
	if($user -and $pwd) {
		Write-Host "using sqlserver auth $user"
		$sqlConnection.ConnectionString = "Server=$server;DataBase=$dbName;Password=$pwd;User ID=$user"
	} else {
		$sqlConnection.ConnectionString = "Server=$server;DataBase=$dbName;Integrated Security=SSPI"
	}
	$sqlConnection.open()

	# check if connection is open
	$sqlConnection | Add-Member IsOpen -MemberType ScriptProperty -Value {
		return $this.State -eq [System.Data.ConnectionState]::Open
	}

	$sqlConnection | Add-Member CommandTimeout -MemberType NoteProperty -Value $CommandTimeout

	# Returns the results of a query (can be multiple rows)
	$sqlConnection | Add-Member Query -MemberType ScriptMethod -Value {
		Param([String]$sql, [System.Data.SqlClient.SqlTransaction]$trans = $null)
		$sqlCmd                = $this.CreateCommand()
		$sqlCmd.CommandText    = $sql
        $sqlCmd.CommandTimeout = $this.CommandTimeout
		if ($trans -ne $null) { $sqlCmd.Transaction = $trans }
		$adapter               = New-Object System.Data.SqlClient.SqlDataAdapter
		$adapter.SelectCommand = $sqlCmd
		$dataSet               = New-Object System.Data.DataSet
		$adapter.Fill($dataSet) | Out-Null
		$sqlCmd.Dispose()
		return $dataSet.Tables[0].Rows
	}

	# Returns one scalar value
	$sqlConnection | Add-Member QueryScalar -MemberType ScriptMethod -Value {
		Param([String]$sql, [System.Data.SqlClient.SqlTransaction]$trans = $null)
		$sqlCmd                = $this.CreateCommand()
		$sqlCmd.CommandTimeout = $this.CommandTimeout
		$sqlCmd.CommandText    = $sql
		if ($trans -ne $null) { $sqlCmd.Transaction = $trans }
		$res = $sqlCmd.ExecuteScalar()
		$sqlCmd.Dispose()
		return $res
	}

	$sqlConnection | Add-Member QueryTable -MemberType ScriptMethod -Value {
		Param([String]$sql, [System.Data.SqlClient.SqlTransaction]$trans = $null)
		$sqlCmd                = $this.CreateCommand()
		$sqlCmd.CommandTimeout = $this.CommandTimeout
		$sqlCmd.CommandText    = $sql
		if ($trans -ne $null) { $sqlCmd.Transaction = $trans }
		$adapter               = New-Object System.Data.SqlClient.SqlDataAdapter
		$adapter.SelectCommand = $sqlCmd
		$dataSet               = New-Object System.Data.DataSet
		$adapter.Fill($dataSet) | Out-Null
		$sqlCmd.Dispose()
		return ,$dataSet.Tables[0]
	}

	# Executes sql query without collecting any result
	$sqlConnection | Add-Member Execute -MemberType ScriptMethod -Value {
		Param([String]$sql, [System.Data.SqlClient.SqlTransaction]$trans = $null)
		$sqlCmd                = $this.CreateCommand()
		$sqlCmd.CommandTimeout = $this.CommandTimeout
		$sqlCmd.CommandText    = $sql
		if ($trans -ne $null) { $sqlCmd.Transaction = $trans }
		$sqlCmd.ExecuteNonQuery() | Out-Null
		$sqlCmd.Dispose()
	}

	# Executes sql query without collecting any result
	$sqlConnection | Add-Member ExecuteProc -MemberType ScriptMethod -Value {
		Param([String]$sql, $params = @(), [System.Data.SqlClient.SqlTransaction]$trans = $null, [int]$Timeout=1000)
		$sqlCmd             = $this.CreateCommand()
		$sqlCmd.CommandType = [System.Data.CommandType]::StoredProcedure
        $sqlCmd.Commandtimeout = $Timeout
		$sqlCmd.CommandText = $sql
		foreach($param in $params) {
			$p = $sqlCmd.Parameters.Add($param.name, $param.type)
			$p.Value = $param.value
		}
		if ($trans -ne $null) { $sqlCmd.Transaction = $trans }
		$sqlCmd.ExecuteNonQuery() | Out-Null
		$sqlCmd.Dispose()
	}

	$sqlConnection | Add-Member ExecuteProcwParam -MemberType ScriptMethod -Value {
		Param($sqlCmd, [System.Data.SqlClient.SqlTransaction]$trans = $null)
		$sqlCmd.Connection = $this
		$sqlCmd.CommandType = [System.Data.CommandType]::StoredProcedure
		$sqlCmd.CommandTimeout = $this.CommandTimeout
		if ($trans -ne $null) { $sqlCmd.Transaction = $trans }
		$sqlCmd.ExecuteNonQuery() | Out-Null
		$sqlCmd.Dispose()
	}


	$sqlConnection | Add-Member BatchInsert -MemberType ScriptMethod -Value {
	    PARAM($table, $query, $params, $data)
	    $adapter                           = New-Object System.Data.SqlClient.SqlDataAdapter
	    $adapter.SelectCommand             = $this.CreateCommand()
	    $adapter.SelectCommand.CommandText = "SELECT * from $table where 1 <> 0"
		$adapter.SelectCommand.UpdatedRowSource =  New-Object  System.Data.UpdateRowSource;

	    $adapter.InsertCommand             = $this.CreateCommand()
	    $adapter.InsertCommand.CommandText = $query

	    foreach($param in $params) {
	        $adapter.InsertCommand.Parameters.Add((New-Object System.Data.SqlClient.SqlParameter -ArgumentList "@$param", $null)) | Out-Null
	        $adapter.InsertCommand.Parameters[$adapter.InsertCommand.Parameters.Count-1].SourceColumn = $param
	    }
	    $adapter.InsertCommand.UpdatedRowSource = New-Object System.Data.UpdateRowSource;
	    $adapter.UpdateBatchSize = 10000


	    $ds = New-Object System.Data.DataSet
	    $dt = New-Object System.Data.DataTable -Args $table
	    $ds.Tables.Add($dt) | Out-Null

    	foreach($param in $params) {
		   	$dt.Columns.Add($param) | Out-Null
		}

	    foreach($row in $data) {
	        $newRow = $dt.NewRow();
	        foreach($param in $params) {
	            $newRow[$param]  = $row[$param]
	        }
	        $dt.Rows.Add($newRow)
	    }
	    $adapter.Update($ds, $table) | Out-Null
	}

	$sqlConnection | Add-Member BatchInsertWithType -MemberType ScriptMethod -Value {
	    PARAM($table, $query, $params, $data)
	    $adapter                           = New-Object System.Data.SqlClient.SqlDataAdapter
	    $adapter.SelectCommand             = $this.CreateCommand()
	    $adapter.SelectCommand.CommandText = "SELECT * from $table where 1 <> 0"
		$adapter.SelectCommand.UpdatedRowSource =  New-Object  System.Data.UpdateRowSource;

	    $adapter.InsertCommand             = $this.CreateCommand()
	    $adapter.InsertCommand.CommandText = $query

	    foreach($row in $params) {
            $param = $row[0]
            $dataType = $row[1]
	        $adapter.InsertCommand.Parameters.Add((New-Object System.Data.SqlClient.SqlParameter -ArgumentList "@$param", $dataType)) | Out-Null
	        $adapter.InsertCommand.Parameters[$adapter.InsertCommand.Parameters.Count-1].SourceColumn = $param
	    }
	    $adapter.InsertCommand.UpdatedRowSource = New-Object System.Data.UpdateRowSource;
	    $adapter.UpdateBatchSize = 10000


	    $ds = New-Object System.Data.DataSet
	    $dt = New-Object System.Data.DataTable -Args $table
	    $ds.Tables.Add($dt) | Out-Null

    	foreach($param in $params) {
		   	$dt.Columns.Add($param[0]) | Out-Null
		}

	    foreach($row in $data) {
	        $newRow = $dt.NewRow();
	        foreach($p in $params) {
                $param = $p[0]
	            $newRow[$param]  = $row[$param]
	        }
	        $dt.Rows.Add($newRow)
	    }
	    $adapter.Update($ds, $table) | Out-Null
	}

	# Works similar to bcp
	# example: $connection.BulkInsert("myTable", @("col1","col2"), @(@{col1=1;col2=2}, @{col1=10, col2=20}))
	$sqlConnection | Add-Member BulkInsert -MemberType ScriptMethod -Value {
	    PARAM($table, $columns, $data)
	    $bulkCopy = New-Object System.Data.SqlClient.SqlBulkCopy -Args $this
	    $bulkCopy.BatchSize = 500;
        $bulkCopy.DestinationTableName = $table;

        if($data -is [System.Data.DataTable]) {
        	$dt = $data
        } else {
		    $dt = New-Object System.Data.DataTable
		   	foreach($column in $columns) {
		   		$dt.Columns.Add($column) | Out-Null
		   	}
		    foreach($row in $data) {
		        $newRow = $dt.NewRow();
		        foreach($column in $columns) {
		            $newRow[$column]  = $row[$column]
		        }
		        $dt.Rows.Add($newRow)
		    }
		}

		foreach($column in $columns) {
			$bulkCopy.ColumnMappings.Add($column, $column) | Out-Null
		}

	    $bulkCopy.WriteToServer($dt) | Out-Null
	}

	
	return $sqlConnection
}

[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.AnalysisServices.AdomdClient") | Out-Null
