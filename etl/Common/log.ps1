
function Write-Log {
	param($connection, $type, $key, $description, $status)
    if(-Not $key) { $key = -1 }
    $maxLength     = 3799
    # if the description is too long we need to split it into multiple entries 
    if($description.length -gt $maxLength) {
      $messageFirst  = "/*[ continued in next log entry ]*/"
      $messageSecond = "/*[ continued from previous log entry ]*/"
      $maxLengthInfo = $maxLength - $messageFirst.length - 1
      Write-Log $connection $type $key "$($description.SubString(0,$maxLengthInfo))$messageFirst" $status 
      Write-Log $connection $type $key "$messageSecond$($description.SubString($maxLengthInfo))"  $status 
    } else {
      $arguments = @(
        @{ name='@pvs_source_type'
           type=[System.Data.SqlDbType]::Varchar
           value=$type},
        @{ name='@pvn_source_key'
           type=[System.Data.SqlDbType]::Int
           value=$key},
        @{ name='@pvs_description'
           type=[System.Data.SqlDbType]::Varchar
           value=$description},
        @{ name='@pvs_status'
           type=[System.Data.SqlDbType]::Varchar
           value=$status}
        )

    $connection.ExecuteProc("etl.csp_AddLog", $arguments)
  }
}

function Register-Log {
    param($levels, $function, $arguments)
    if(-Not $global:ps_logger) {
        $global:ps_logger = @{}
    }
    foreach($level in $levels) {
        if(-Not $global:ps_logger.ContainsKey($level)) {
            $global:ps_logger.Add($level, @())
        }
        if($function -notin $global:ps_logger[$level].function) {
            $global:ps_logger[$level] += @{
                function  = $function
                arguments = $arguments
            }
        }
    }
}

function UnRegister-AllLoggers {
    $global:ps_logger = $null
}

function Write-LogLevel {
    param($level, $text)
    if(-Not $global:ps_logger) {
        return;
    }

    $loggers = $global:ps_logger[$level]
    foreach($logger in $loggers) {
        $arguments = $logger.arguments
        try {
            & $logger.function -level $level -text $text @arguments @args
        } catch {
            Write-Warning "Logger $($logger.function) failed : $_"
        }
    }
}

function Log-Info  { Write-LogLevel "info"  @args }
function Log-Debug { Write-LogLevel "debug" @args }
function Log-Warn  { Write-LogLevel "warn"  @args }
function Log-Error { Write-LogLevel "err"   @args }

function Log-StdOut {
    param($level, $text)
    $currtime = Get-Date -format "MM/dd/yyyy HH:mm:ss"
    Write-Host "[$currtime][$level] $text"
}

function Log-File {
    param($level, $text, $thread, $dir, $name)
    Write-LogFile -dir $dir -name $name -log "[$level] $text"
}

function Write-Logfile {
    param($dir, $name, $log, $threadNumber)

    if(-not [System.IO.Directory]::Exists($dir)) {
        New-Item -ItemType Directory -Force -Path $dir | Out-Null
    }

    if(-Not $threadNumber) {
        $logfile=Join-Path $dir "$name.log"
        $message = $log
    } else {
        $logfile= Join-Path $dir "$name$threadNumber.log"
        $message = "[Thread $threadNumber]$log"
    }

    Check-LogFile $logfile
    ## Wait for one minute (60s=5s*12times), if still lock then put to next time
    if((Test-FileLockEx $logfile '5' '12') -ne 1) {
        throw "File lock timeout on logfile $logfile!"
    }
    $currtime = Get-Date -format "MM/dd/yyyy HH:mm:ss"
    if($message -ne '.') {
        $logs = '[' + $currtime + ']' + $message
    } else {
        $logs = $message
    }

    Add-Content  $logfile -Value $logs -Encoding UTF8 | Out-Null
}


function Check-LogFile {
    param([string]$logfile)
    if(Test-Path $logfile){
        if((Get-Item $logfile).length -gt 100MB)
        {
            # logfile is too huge (greater than 500MB), recreate it now.
            [string]$logfile_bak = $logfile + "_history.log"
            if(Test-Path $logfile_bak){
                Remove-Item -Force -ErrorAction SilentlyContinue -LiteralPath $logfile_bak
            }
            Rename-Item -Force -ErrorAction SilentlyContinue -Path $logfile -NewName $logfile_bak | Out-Null
            New-Item -Path $logfile -ItemType file -Force | Out-Null
        }
    } else {
        New-Item -Path $logfile -ItemType file -Force | Out-Null
    }
}

function Test-FileLock {
    param([string]$fileName)

    $filelocked = $false
    $fileInfo = New-Object System.IO.FileInfo $fileName

    trap {
        Set-Variable -name filelocked -value $true -scope 1
        continue
    }

    $fileStream = $fileInfo.Open( [System.IO.FileMode]::OpenOrCreate, [System.IO.FileAccess]::ReadWrite, [System.IO.FileShare]::None )
    if ($fileStream) {
        $fileStream.Close()
    }

    return $filelocked
}

function Test-FileLockEx {
    param($file, $sec, $times)
    $f_check_passed = $true
    if (Test-FileLock($file) -eq $true)
    {
        #File lock detected for $($file), waiting for the lock to be released...
        $f_check_passed = $false
        for ($i = 1; $i -le $times; $i++)
        {
            if ((Test-FileLock $file) -eq $false)
            {
                $f_check_passed = $true
                #File $($file) unlocked, continue processing.
                break
            }
            Start-Sleep -s $sec
        }
    }
    if ($f_check_passed -eq $true){
        return 1
    }
    else
    {
        #Timeout while waiting for file $($file) to be unlocked, so postpone the processing of this file to next time.
        return 0
    }
}
