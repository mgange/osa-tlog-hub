Param(
   $dbServer=".\sql2014"
 , $dbName="HUB_POC"
 , $run=$true
)

. "$PSScriptRoot\Tables.ps1"
. "$PSScriptRoot\..\common\common.ps1"
. "$PSScriptRoot\..\common\connect.ps1"
. "$PSScriptRoot\..\common\log.ps1"
. "$PSScriptRoot\..\common\config.ps1"

$ErrorActionPreference = "STOP"
$source_key=999999

function Import-Config {
    param($dbConn, $dbName, $config, $configFolder)

    if(-Not $configFolder) {
        $configFolder  = $config.GetProperty("deploy.metadata.dir")
    }

    $tables = Get-ConfigTables -dbName $dbName

    foreach($table in $tables) {
        $cfgFile = Join-Path $configFolder $table.file
       
		if($table.delimiter -ne $null){
			$delimiter = "'$($table.delimiter)'"
		}
		else {
			$delimiter = " '\t' "
		}
        if(-Not (Test-Path $cfgFile)) {
            Throw "Missing config file: $cfgFile"
            # error message to the database and std_out
            $text = "ImportConfig.ps1: Missing config file: $cfgFile"
            Log-StdOut "err" $text
            Log-Error $text
            Write-Log $dbConn "Import Config" $source_key $text "ERROR"
        }
        $dbConn.Execute("truncate table $($table.name)");

        # user and message for user
        $text = "Starting import"
        Write-Log $dbConn "Import Config" $source_key $text "COMPLETE"
        $check = 1
        try {
            $text = "Import $cfgFile to $dbName"
            Log-Info $text

            $text = "Import $cfgFile to $dbName in progress"
            Write-Log $dbConn "Import Config" $source_key $text "IN PROGRESS"
            Log-Info $text

            $sqlt = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'vwTmpview') AND type in (N'V')) 
	                    DROP VIEW vwTmpview;"
            $dbConn.Execute($sqlt)

			$sql = "CREATE VIEW vwTmpview AS SELECT $($table.columns) FROM $($table.name);"
            $dbConn.Execute($sql)

            $rt = [char]13 + [char]10
			$sql =	" BULK INSERT vwTmpview FROM '$cfgFile' WITH (FIRSTROW = 2,DATAFILETYPE = 'char',FIELDTERMINATOR = $delimiter, ROWTERMINATOR = '$rt');"
            $dbConn.Execute($sql)
            $dbConn.Execute($sqlt)
            $check = 0

        } catch {
            $check = 1
            $text = "Import $cfgFile to $dbName"
            write-host $table.name + "(" + $table.columns + ")"
            Write-Log $dbConn "Import Config" $source_key $text "ERROR"
            Throw
        }

        If ($check -eq 0){
          $text = "Import $cfgFile to $dbName successfully completed"
          write-host $table.name + "(" + $table.columns + ")"
          Write-Log $dbConn "Import Config" $source_key $text "COMPLETED"
        }
    }

}

# Code is executed here
if($run) {
    try {
        $dbConn = Create-SqlConnection $dbServer $dbName
        $config = Get-Config -connection $dbConn -dbName $dbName
        # logging
        $text = "Import Config Initiated for $dbServer and $dbName"
        Log-Info "Start Import Config"
        Write-Log $dbConn "Import Config" $source_key $text "STARTED"
        Import-Config -dbConn $dbConn -dbName $dbName -config $config
        Log-Info "End of Import Config"
        Write-Log $dbConn "Import Config" $source_key $text "COMPLETED"
        Exit 0
    } catch {
        Print-Error
        # logging
        Log-Info "Import Config Failed for $dbServer and $dbName "
        $text = "Import Config Failed for $dbServer and $dbName "
        Write-Log $dbConn "Import Config" $source_key $text "ERROR"
        Log-Error $text
        Exit 1
    } finally {
        if($dbConn) {
            $dbConn.close()
        }
    }
}