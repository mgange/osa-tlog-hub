function Get-ConfigTables {
	param($dbName)
	return @(
        @{file="1.FilemoverServer.csv"; name="metadata.FilemoverServer"; columns="ServerName, ServerType, ServerAddress, PortNo, Username, Password, RootDirectory, UploadDirectory, InprogressUploadDirectory, Rename, Active, Recursive"},
        @{file="2.FilemoverMapping.csv"; name="metadata.FilemoverMapping"; columns="SourceServerName, SourcePath, TargetServerName, FilePattern, KeepSubFolder,DeleteSource, Compress, Priority, BackupFolder, BackupDaysToKeep"}
    )
}