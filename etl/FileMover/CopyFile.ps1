[Reflection.Assembly]::LoadFrom("$PSScriptRoot\..\common\thirdparty\Renci.SshNet.dll") | Out-Null
. "$PSScriptRoot\..\common\Crypto.ps1"
. "$PSScriptRoot\FindNewFiles.ps1"

function Create-FTPDirectory {
    param($sftp, $path, $subfolder)
    # calculate the root path (we shouldn't create the HUB_TEST/active folder for example)
    # minus one to remove the trailing /
	# split all the sub folders
    if($subfolder -eq '' -or $subfolder -eq $null){
        $root = '/'
        $directories = $path -split '/'
    }
    else { 
        $root = $path.SubString(0,$path.IndexOf($subfolder)-1)
        $directories = $subfolder -split '/'
    }
	
    $currentPath = $null
    foreach($dir in $directories) {
        if(-Not $currentPath) {
            $currentPath = $dir
        } else {
            $currentPath = "$currentPath/$dir"
        }
		$fileExists = Check-File-Or-Directory-Exists -ssh $ssh -directory "$root/$currentPath" -type "D" 
        if(-Not $fileExists){
            Log-Info "Create sub directory: $root/$currentPath"
            $sftp.CreateDirectory("$root/$currentPath")
        }
    }
}

function Copy-File-SMBToFTP {
    param($address, $username, $password, $srcFile, $destPath, $tempPath, $backupdir_dict,  $filename, $subfolder, $config)
    Log-Info "`Copy $srcFile to $destPath/$filename @ $address"
    try {
        $sftp = New-Object Renci.SshNet.SftpClient($address, $username, (Decrypt $password))
        $ssh = New-Object Renci.SshNet.SshClient($address, $username, (Decrypt $password))
        $ssh.KeepAliveInterval = New-TimeSpan -Seconds 300
        $maxTry = 5
        for($i = 0; $i -lt $maxTry;  $i++ ){
            try{
                if( -not $sftp.IsConnected){
                    $sftp.Connect();
                }
                if( -not $ssh.IsConnected){
                    $ssh.Connect()
                }
                break
            }
            catch{
                Start-Sleep -s $i
                Log-Info "the $i try to ssh/sftp shared node."
                if($i -eq $maxTry-1){
                    $msg = "the $maxTry times try to ssh/sftp shared node failed, quit the job"
                    Log-Info $msg 
                    throw
                }
            }
        }

        # make sure that the any eventual subfolders are created
        if($backupdir_dict.type -eq "FTP"){
		    $fileExists = Check-File-Or-Directory-Exists -ssh $ssh -directory $backupdir_dict.dir -type "D"
            if(-Not $fileExists) {
                Log-Info "Create directory $($backupdir_dict.dir)"
                Create-FTPDirectory -sftp $sftp -path $backupdir_dict.dir -subfolder $subfolder
            }
        }
        $lastWriteTime = Get-Date -Format "yyyyMMddHHmm.ss" (Get-ChildItem -literalpath $srcFile).LastWriteTime
        $stream = New-Object IO.FileStream $srcFile , 'Open', 'Read'

        # does the directory already exists on target server
		$fileExists = Check-File-Or-Directory-Exists -ssh $ssh -directory $destPath -type "D" 
        if(-Not $fileExists) {
            Log-Info "Create directory $destPath"
            Create-FTPDirectory -sftp $sftp -path $destPath -subfolder $subfolder
        }
        # does the file already exists on the target server?
        $fileExists = Check-File-Or-Directory-Exists -ssh $ssh -directory $destPath -file $filename -type "F"
        if($fileExists) {
            Log-Info "filename exists, add timestamp"
            $filename = Add-TimeStamp $filename
        }

        # are we using a temporary location
        $retrylimit = 5
        $uploaded = $false

        for($count=0; -not $uploaded -and $count -lt $retrylimit; $count++){
            try{
                if($tempPath -ne $destPath) {
                    $sftp.UploadFile($stream, "$tempPath/$filename");
                    $sftp.Get("$tempPath/$filename").MoveTo("$destPath/$filename")
                } else {
                    $sftp.UploadFile($stream, "$destPath/$filename");
                }
                $uploaded = $true
            }
            catch{
                if($count -ge $retrylimit -1 ){
                    Log-Info "Error: FileMove upload retry limit has been reached!"
					throw
                }
                else {
                    Start-Sleep -Milliseconds (5000 * $count)
                }
            }
        }

        $cmd = $ssh.RunCommand("touch -t `'$lastWriteTime`' `'$destPath/$filename`'")
        if($cmd.ExitStatus -ne 0) {
            throw "failed with $($cmd.Error)" 
        }

        # Copying the same file to Backup location
        if ($backupdir_dict.type -eq "FTP") {
           $cmd = $ssh.RunCommand("cp `'$destPath/$filename`' `'$($backupdir_dict.dir)/$filename`'")
           if($cmd.ExitStatus -ne 0) {
             throw "failed with $($cmd.Error)" 

           } else {
                Log-Info "File '$filename' copied to Backup Directory of FTP server"
           }
        }
        else {
            # Copy file from local temp to destination SMB backup location
            $backupdir = $backupdir_dict.dir
            $upperBackupFolder = ($backupdir -split '\\')[0..(($backupdir -split '\\').Count-2)] -join '\'

            if(-not (Test-Path -literalpath $backupdir)) {
                try{
                    [System.IO.Directory]::CreateDirectory($backupdir) | Out-Null
                }
                catch{
                    throw "backup_folder $backupdir is not valid!"
                }
            }
            Copy-Item -Path $srcFile -Destination $backupdir -Force
        }

    } catch {
        Log-Info "exception: $_"
        Throw
    } finally {
        if($stream) {
            $stream.close();
        }
        $sftp.Disconnect();
		$ssh.Disconnect();
    }
}

function Copy-File-FTPToFTP{
    # param($address, $username, $password, $srcFile, $destPath, $tempPath, $backupdir,  $filename, $subfolder)
    param([ref]$src, [ref]$dst, $srcFile, $destPath, $tempPath, $backupdir_dict,  $filename, $subfolder, $updateDate)
    Log-Info "`Copy $srcFile to $destPath/$filename @ $address"
    try {
        $src_address = $src.value.address
        $src_username = $src.value.username
        $src_password = $src.value.password
        $address = $dst.value.address
        $username = $dst.value.username
        $password = $dst.value.password
        $src_sftp = New-Object Renci.SshNet.SftpClient($src_address, $src_username, (Decrypt $src_password))
        $sftp = New-Object Renci.SshNet.SftpClient($address, $username, (Decrypt $password))
        $ssh = New-Object Renci.SshNet.SshClient($address, $username, (Decrypt $password))
        $ssh.KeepAliveInterval = New-TimeSpan -Seconds 300
        $maxTry = 5
        for($i = 0; $i -lt $maxTry;  $i++ ){
            try{
                $src_sftp.Connect();
                $sftp.Connect();
                $ssh.Connect();
                break
            }
            catch{
                if($i -eq $maxTry-1){
                    throw
                }
                Start-Sleep -s $i
                Log-Info "the $i try to ssh/sftp shared node."
            }
        }

        if($ssh -eq $null -or $sftp -eq $null -or $src_sftp -eq $null){
            $msg = "the $maxTry times try to ssh/sftp shared node failed, quit the job"
            Log-Info 
            throw $msg
        }
        # make sure that the any eventual subfolders are created
        $fileExists = Check-File-Or-Directory-Exists -ssh $ssh -directory $destPath -type "D" 
        if(-Not $fileExists) {
            Log-Info "Create directory $destPath"
            Create-FTPDirectory -sftp $sftp -path $destPath -subfolder $subfolder
        }
        if($backupdir_dict.type -eq "FTP"){
		    $fileExists = Check-File-Or-Directory-Exists -ssh $ssh -directory $backupdir_dict.dir -type "D"
            if(-Not $fileExists) {
                Log-Info "Create directory $($backupdir_dict.dir)"
                Create-FTPDirectory -sftp $sftp -path $backupdir_dict.dir -subfolder $subfolder
            }
        }

        $lastWriteTime = Get-Date -Format "yyyyMMddhhmm.ss" $updateDate
        # Download file to local temp table
        $downloadTmpFolder = $env:TEMP 
        $localTmpFile = @($downloadTmpFolder,$filename) -join '\'
        Log-Info "Download file to temp folder: $localTmpFile "
        $stream = New-Object IO.FileStream $localTmpFile , 'OpenOrCreate'
        $maxTry = 5
        for($i = 0; $i -lt $maxTry;  $i++ ){
            try{
                $src_sftp.DownloadFile($srcFile, $stream);
                break
            }
            catch{
                if($i -eq $maxTry-1){
                    throw
                }
                Start-Sleep -s $i
                Log-Info "the $i try to download $localTmpFile from $src_address."
            }
        }
        

        # does the file already exists on the server?
        $fileExists = Check-File-Or-Directory-Exists -ssh $ssh -directory $destPath -file $filename -type "F"
        if($fileExists) {
            Log-Info "filename exists, add timestamp"
            $filename = Add-TimeStamp $filename
        }

        # are we using a temporary location
        $retrylimit = 5
        $uploaded = $false

        for($count=0; -not $uploaded -and $count -lt $retrylimit; $count++){
            try{
                if($tempPath -ne $destPath) {
                    $sftp.UploadFile($stream, "$tempPath/$filename");
                    $sftp.Get("$tempPath/$filename").MoveTo("$destPath/$filename")
                } else {
                    $sftp.UploadFile($stream, "$destPath/$filename");
                }
                $uploaded = $true
                $stream.Close()
            }
            catch{
                if($count -ge $retrylimit -1 ){
                    Log-Info "Error: FileMove upload retry limit has been reached!"
                    throw
                }
                else {
                    Start-Sleep -Milliseconds (5000 * $count)
                }
            }
        }

        $cmd = $ssh.RunCommand("touch -t `'$lastWriteTime`' `'$destPath/$filename`'")
        if($cmd.ExitStatus -ne 0) {
            throw "failed with $($cmd.Error)" 
        }

        # Copying the same file to Backup location
        if ($backupdir_dict.type -eq "FTP")  {
           $cmd = $ssh.RunCommand("cp `'$destPath/$filename`' `'$($backupdir_dict).dir/$filename`'")
           if($cmd.ExitStatus -ne 0) {
             throw "failed with $($cmd.Error)" 

           } else {
                Log-Info "File '$filename' copied to Backup Directory of FTP server"
           }
        }
        else {
            # Copy file from local temp to destination SMB backup location
            if(-Not (Test-Path -literalpath $backupdir_dict.dir)) {
                try{
					[System.IO.Directory]::CreateDirectory($backupdir_dict.dir) | Out-Null
                }
                catch{
                    throw "backup_folder $($backupdir_dict.dir) is not valid!"
                }
            }
            Copy-Item -Path $localTmpFile -Destination $backupdir_dict.dir -Force
        }

        # Delete downloaded temp files
        RemoveFileFromSMBServer -fullPath $localTmpFile

    } catch {
        Log-Info "exception: $_"
        Throw
    } finally {
        if($stream) {
            $stream.close();
        }
        $src_sftp.Disconnect();
        $sftp.Disconnect();
        $ssh.Disconnect();
    }
}

function Copy-File-SMBToSMB{
    param($srcFile, $destPath, $backupdir_dict,  $filename, $subfolder, $config, $compress)
    Log-Info "`Copy $srcFile to $destPath/$subfolder/$filename "
    try {
        function Copy-File-SMBToSMB-Private{
            param($srcFile, $destPath, $filename, $subfolder, $config)
            $upperDestPath = ($destPath -split '\\')[0..(($destPath -split '\\').Count-2)] -join '\'
            $fullDestPath = @($destPath, $subfolder) -join '\'
            if(-not (Test-Path -literalpath $fullDestPath)) {
                try{
                    [System.IO.Directory]::CreateDirectory($fullDestPath) | Out-Null
                }
                catch{
                    throw "Path $fullDestPath is not valid!"
                }
            }
            Copy-Item -Path $srcFile -Destination $fullDestPath -Force
        }
		function Zip-Directory {
			Param(
			  [Parameter(Mandatory=$True)][string]$DestinationFileName,
			  [Parameter(Mandatory=$True)][string]$SourceDirectory,
			  [Parameter(Mandatory=$False)][string]$CompressionLevel = "Optimal",
			  [Parameter(Mandatory=$False)][switch]$IncludeParentDir
			)
			Add-Type -AssemblyName System.IO.Compression.FileSystem
			$CompressionLevel    = [System.IO.Compression.CompressionLevel]::$CompressionLevel  
			[System.IO.Compression.ZipFile]::CreateFromDirectory($SourceDirectory, $DestinationFileName, $CompressionLevel, $IncludeParentDir)
		}
		
		$ext = $filename.split(".")[-1]
		$name = ($filename.Split('.'))[0..(($filename.Split('.')).Count-2)] -join '.'
		if($compress -eq 1 -and $ext -notin @('zip','rar','7z')){
			$zipFolder    = @($destPath, $name) -join '\'
			Copy-File-SMBToSMB-Private -srcFile   $srcFile `
								-destPath  $zipFolder `
								-subfolder $subfolder `
								-config $config
			$tmp = (@($name, 'zip') -join '.').ToString()
			$zipFile = @($destPath, $tmp) -join '\'
			$Compression     = "Optimal"  # Optimal, Fastest, NoCompression
			
            Log-Info "`Compressing $srcFile to $destPath/$subfolder/$zipFile, Compression $Compression "

			Zip-Directory -DestinationFileName "$zipFile" `
				-SourceDirectory "$zipFolder" `
				-CompressionLevel $Compression 
			$srcFile = $zipFile 
			Remove-Item -Recurse -Force $zipFolder
		} else{
            # Copying the file to $destPath/$subfolder
            Copy-File-SMBToSMB-Private -srcFile   $srcFile `
                                -destPath  $destPath `
                                -subfolder $subfolder `
                                -config $config
        }

        # CopyingCopy-File-SMBToSMB-Privatethe same file to Backup location
        if ($backupdir_dict.type -eq "SMB") {
            # Copy file from local temp to destination SMB backup location
            $backupdir = $backupdir_dict.dir
            Log-Info "`Backup $srcFile to $backupdir/$filename "

            Copy-File-SMBToSMB-Private -srcFile   $srcFile `
                                -destPath  $backupdir `
                                -config $config
        }

    } catch {
        Log-Info "exception: $_"
        if(Test-Path $zipFolder){
            Remove-Item -Recurse -Force $zipFolder
        }
        Throw
    } 
}

function Copy-File-FTPToSMB{
    Log-Info "Not available yet"
}

function Move-RecoverFile {
    param($address, $username, $password, $srcFile, $destPath, $tempPath, $backupdir,  $filename, $subfolder)
    Log-Info "`Move recover file to $destPath/$filename @ $address"
    try {
        $sftp = New-Object Renci.SshNet.SftpClient($address, $username, (Decrypt $password))
        $sftp.Connect();
        $ssh = New-Object Renci.SshNet.SshClient($address, $username, (Decrypt $password))
		$ssh.KeepAliveInterval = New-TimeSpan -Seconds 300
        $ssh.Connect()
		$filePath = ""
		$paths = @()

        # make sure that the any eventual subfolders are created
        # make sure that the any eventual subfolders are created
		$fileExists = Check-File-Or-Directory-Exists -ssh $ssh -directory $destPath -type "D" 
        if(-Not $fileExists) {
            Log-Info "Create directory $destPath"
            Create-FTPDirectory -sftp $sftp -path $destPath -subfolder $subfolder
        }
		$fileExists = Check-File-Or-Directory-Exists -ssh $ssh -directory $backupdir -type "D"
        if(-Not $fileExists) {
            Log-Info "Create directory $backupdir"
            Create-FTPDirectory -sftp $sftp -path $backupdir -subfolder $subfolder
        }

		if($tempPath -ne $destPath) {
			$paths = @("$tempPath", "$destPath", "$backupdir")
		}
		else {
			$paths = @($destPath, $backupdir)
		}
			
		foreach($path in $paths) {
			$fileExists = Check-File-Or-Directory-Exists -ssh $ssh -directory $path -file $filename -type "F"
			if($fileExists) {
				$filePath = "$path/$filename"
				break
			}
		}
		
		foreach($path in $paths) {
			$fileExists = Check-File-Or-Directory-Exists -ssh $ssh -directory $path -file $filename -type "F"
			if(-not $fileExists) {
				$path = "$path/$filename"
				$cmd = $ssh.RunCommand("cp `'$filePath`' `'$path`'")
				if($cmd.ExitStatus -ne 0) {
            		throw "failed with $($cmd.Error)" 
				}
			}
		}
		
		if($tempPath -ne $destPath) {
			$fileExists = Check-File-Or-Directory-Exists -ssh $ssh -directory $tempPath -file $filename -type "F"
			if($fileExists){
				$cmd = $ssh.RunCommand("rm `'$tempPath/$filename`'")
				if($cmd.ExitStatus -ne 0) {
            		throw "failed with $($cmd.Error)" 
				}
			}
		}

    } catch {
        Log-Info "exception: $_"
        Throw
    } finally {
        $sftp.Disconnect();
		$ssh.Disconnect();
    }
}

function Update-AvailableFilesStatus {
    param($dbConn, $fileId, $targetServer, $status, $message)
    $message = $message -replace "'", "''"
    $dbConn.Execute("UPDATE etl.FilemoverStatus
        SET Status = '$status', StatusReason = '$message', UpdateDate  = GetDate()
        WHERE FileID = $fileId AND TargetServerName = '$targetServer'")
}

function Copy-File {
    param($dbConn, $config, $fileId, $fullPath, $srcServer, $dstServer, $subfolder, $updateDate, $backupdir)
    try {
        $src = $dbConn.QueryTable("SELECT * FROM metadata.FilemoverServer where ServerName = '$srcServer'")
        $dst = $dbConn.QueryTable("SELECT * FROM metadata.FilemoverServer where ServerName = '$dstServer'")
		$compress = $dbConn.QueryTable("SELECT top 1 compress FROM metadata.FilemoverMapping where SourceServerName = '$srcServer' and TargetServerName = '$dstServer'").compress

        $copyType = [System.Tuple]::Create($src.ServerType,$dst.ServerType)
        $smb2ftp = [System.Tuple]::Create("SMB", "FTP")
        $smb2smb = [System.Tuple]::Create("SMB", "SMB")
        $ftp2smb = [System.Tuple]::Create("FTP", "SMB")
        $ftp2ftp = [System.Tuple]::Create("FTP", "FTP")
        if($copyType -notin @($smb2ftp, $ftp2ftp, $smb2smb)) {
            Throw "FileMover supports only $smb2ftp, $smb2smb and $ftp2ftp file movement. Current copytype $copyType is not supported!"
        }
        
        # Currently, we support only three types of file movement
        if($copyType -in @($smb2ftp, $ftp2ftp, $smb2smb)){
            if($backupdir -eq $null -or $backupdir -eq ''){
                $backupdir = "$($dst.RootDirectory)/backup/$subfolder"
                $backupdir_type = "FTP"
            }
            else{
                $backupdir_type = "SMB"
            }
            if($dst.IsNull('UploadDirectory')) {
                $destPath = "$($dst.RootDirectory)/$subfolder"
            } else {
                $destPath = "$($dst.RootDirectory)/$($dst.UploadDirectory)/$subfolder"
                $tdir = $dst.UploadDirectory
                if($tdir.Contains("active/")){
                        $tdir = $tdir.Replace("active/", '')
                        if($backupdir -eq "$($dst.RootDirectory)/backup/$subfolder"){
                            $backupdir = "$($dst.RootDirectory)/backup/$tdir/$subfolder"
                        }
                }
            }
            if($dst.IsNull('InprogressUploadDirectory')) {
                $tempPath = $destPath
            } else {
                $tempPath = "$($dst.RootDirectory)/$($dst.InprogressUploadDirectory)"
            }
            $backupdir_dict = @{dir=$backupdir; type=$backupdir_type}
        }

        switch($copyType){
            $smb2ftp {
                $fileName = Split-Path -Leaf $fullPath
                if(Test-Path -literalpath $fullPath) {
                    Copy-File-SMBToFTP -address   $dst.address `
                                -username  $dst.username `
                                -password  $dst.password `
                                -srcFile   $fullPath `
                                -destPath  $destPath `
                                -tempPath  $tempPath `
                                -fileName  $fileName `
                                -subfolder $subfolder `
                                -backupdir_dict $backupdir_dict `
                                -config $config
                }
                else{
                    if($backupdir_dict.type -eq 'FTP'){
                        Move-RecoverFile -address   $dst.address `
                                    -username  $dst.username `
                                    -password  $dst.password `
                                    -srcFile   $fullPath `
                                    -destPath  $destPath `
                                    -tempPath  $tempPath `
                                    -fileName  $fileName `
                                    -subfolder $subfolder `
                                    -backupdir $backupdir_dict.dir 
                    }          
                }
                break
            }
            $ftp2ftp {
                # First download file from source ftp server to a local system temporary folder 
                # and then upload it to target ftp server
                $fileName = Split-Path -Leaf $fullPath
                Copy-File-FTPToFTP -dst   ([ref]$dst)`
                            -src  ([ref]$src) `
                            -srcFile   $fullPath `
                            -destPath  $destPath `
                            -tempPath  $tempPath `
                            -fileName  $fileName `
                            -subfolder $subfolder `
                            -backupdir_dict $backupdir_dict `
                            -updateDate $updateDate
                break
                
            }
            $smb2smb {
                $fileName = Split-Path -Leaf $fullPath
                Copy-File-SMBToSMB -srcFile   $fullPath `
                            -destPath  $destPath `
                            -fileName  $fileName `
                            -subfolder $subfolder `
                            -backupdir_dict $backupdir_dict `
                            -config $config `
							-compress $compress
                break
                 
            }
            $ftp2smb {
                Copy-File-FTPToSMB 
            }
        }
        Update-AvailableFilesStatus -dbConn $dbConn -fileId $fileId -targetServer $dstServer -status 'COMPLETE'
		

    } catch {
        Update-AvailableFilesStatus -dbConn $dbConn -fileId $fileId -targetServer $dstServer -status 'ERROR' -message $_
    }
}

function Get-SubFolder {
    param($fullPath, $sourcePath, $rootPath)
    if(-Not $fullPath.Contains($sourcePath)) {
        Throw "unable to extract subfolder from $sourcePath and $fullPath"
    }

    $filename  = [io.path]::GetFileName($fullPath)
    # the full name should be root / sourcePath / subfolder1 / subfolder2 ... / fileName
    # sourcePath might be '', and there can be 0 or more subfolders, it's these subfolders we are trying to extract
    # and since this should work both for windows and unix, we are looking at both \ and / as path seperators
    if(-Not ($fullPath -match "$([Regex]::Escape($rootPath))[/,\\]?$([Regex]::Escape($sourcePath))[/,\\]?(?<subfolder>.*?)[/,\\]?$([Regex]::Escape($filename))`$")) {
        Throw "unable to extract subfolder from $sourcePath and $fullPath"
    }
    return $Matches['subfolder'] -replace '\\','/'
}

function Copy-AllAvailableFiles {
	param($dbConn, $config)
    # get all files that don't have any FILEMOVER_STATUS records but do have mapping.
	$files = $dbConn.QueryTable("SELECT a.FileID,a.FullPath,a.FileLastModified,a.SourcePath,
        m.SourceServerName,m.TargetServerName,m.BackupFolder,m.KeepSubFolder,m.DeleteSource,
        ss.ServerType AS SourceServerType,ss.ServerAddress AS SourceServerAddress,ss.Username AS SourceUsername,ss.Password AS SourcePassword,ss.RootDirectory AS SourceRoot 
        FROM etl.FilemoverAvailableFiles a
        INNER JOIN metadata.FilemoverMapping m 
	        ON m.SourceServerName = a.ServerName
            AND dbo.fn_RegexMatch(m.FilePattern,a.FullPath) = 1
            AND ISNULL(a.SourcePath,'') = ISNULL(m.SourcePath,'')
        INNER JOIN metadata.FilemoverServer ss 
	        ON ss.ServerName = a.ServerName
	        AND ss.active = 1
        INNER JOIN metadata.FilemoverServer ts 
	        ON ts.ServerName = m.TargetServerName
	        AND ts.active = 1
        WHERE NOT EXISTS (SELECT NULL FROM [etl].[FilemoverStatus] s where s.FileID = a.FileID and s.TargetServerName = ts.ServerName and status <> 'RECOVER_IN_PROGRESS')
        ORDER BY m.Priority;")

    if($files -eq $null -or $files.Rows.Count -eq 0) {
        Log-Info "No files to upload or recover"
        return;
    }

    Log-Info "Files to upload or recover: $($files.Rows.Count)"
	foreach($file in $files) {
        Log-Info "Starting - FileID: $($file.FileID), FullPath: $($file.FullPath)"
        if((-Not $file.IsNull('KeepSubFolder')) -And $file.KeepSubFolder) {
            $subfolder = Get-SubFolder -fullPath $file.FullPath -sourcePath $file.SourcePath -rootPath $file.SourceRoot
        } else {
            $subfolder = $null
        }

        $dbConn.Execute("IF NOT EXISTS (SELECT 1 FROM etl.FilemoverStatus WHERE FileID = $($file.FileID) AND TargetServerName = '$($file.TargetServerName)')
					  BEGIN
					  	INSERT INTO etl.FilemoverStatus(FileID, TargetServerName, Status, UpdateDate) VALUES($($file.FileID), '$($file.TargetServerName)', 'IN PROGRESS', getdate())
					  END")

        Log-Info "Copy-File params: -fileId $($file.FileID) -fullPath $($file.FullPath) -srcServer $($file.SourceServerName) -dstServer $($file.TargetServerName) -subfolder  $subfolder -updateDate $($file.FileLastModified) -backupdir $($file.BackupFolder)"

		Copy-File -dbConn $dbConn `
                  -config $config `
                  -fileId     $file.FileID `
                  -fullPath   $file.FullPath `
                  -srcServer  $file.SourceServerName `
                  -dstServer  $file.TargetServerName `
                  -subfolder  $subfolder `
                  -updateDate $file.FileLastModified `
                  -backupdir  $file.BackupFolder
		
		
		if((-Not $file.IsNull('DeleteSource')) -And $file.DeleteSource){
            if($file.SourceServerType -eq 'SMB'){
			     RemoveFileFromSMBServer -fullPath $file.FullPath
            }
            elseif($file.SourceServerType -eq 'FTP'){
                 RemoveFileFromFTPServer -address $file.SourceServerAddress -username $file.SourceUsername -password $file.SourcePassword `
                    -fullPath $file.FullPath
            }
		}
	}
}

function RemoveFileFromSMBServer{
	param($fullPath)
	if(Test-Path -literalpath $fullPath){
		Remove-Item -literalpath $fullPath
	}
}

function RemoveFileFromFTPServer{
    param($address, $username, $password, $fullPath)
    try{
        $sftp = New-Object Renci.SshNet.SftpClient($address, $username, (Decrypt $password))
        $sftp.Connect()
        $sftp.DeleteFile($fullPath)
    } catch {
        Log-Info "exception: $_"
        Throw
    } finally {
        $sftp.Disconnect();
    }
}

function Check-File-Or-Directory-Exists{
	param($ssh = $null, $file = $null, $directory = $null, $type = $null)
	$return = $true
	if($type -eq "F"){
		$file = Escape-SSH $file
		$cmd = "[ -f $directory/$file ] && echo '0'|| echo '1'"
		$results = $ssh.RunCommand($cmd)   
		if($results) {
			if($results.Result.Trim() -eq "0"){
				$return = $true
			}else{
				$return = $false
			}
		}
    }
	
	if($type -eq "D"){
		$cmd = "[ -d $directory ] && echo '0'|| echo '1'"
		$results = $ssh.RunCommand($cmd)   
		if($results) {
			if($results.Result.Trim() -eq "0"){
				$return = $true
			}else{
				$return = $false
			}
		}
    }
		
	return $return
}

function Escape-SSH {
  param($file) 
  $file = $file -replace '\$', '\$'
  $file = $file -replace ' ', '\ '
  $file = $file -replace '&', '\&'
  $file = $file -replace '\(', '\('
  $file = $file -replace '\)', '\)'
  $file = $file -replace '\+', '\+'
  return "''$file''"
}