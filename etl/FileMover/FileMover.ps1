. "$PSScriptRoot\UpdateAvailable.ps1"
. "$PSScriptRoot\CopyFile.ps1"
. "$PSScriptRoot\FindNewFiles.ps1"
. "$PSScriptRoot\..\common\common.ps1"
. "$PSScriptRoot\..\common\config.ps1"
. "$PSScriptRoot\..\common\Crypto.ps1"

$ErrorActionPreference = "Stop"

function Get-Servers {
<#
    Returns all active source servers used in the mapping
#>
    PARAM($dbConn)
    $dbConn.QueryTable('SELECT * FROM metadata.FilemoverServer s WHERE s.Active = 1 AND EXISTS (SELECT top 1 NULL from metadata.FilemoverMapping m WHERE m.SourceServerName = s.ServerName)')
}

function Get-ServerMapping {
    param($dbConn, $serverName)
    $rows = $dbConn.QueryTable("SELECT * from metadata.FilemoverMapping WHERE SourceServerName = '$serverName' ORDER BY len(ISNULL(SourcePath,'')) desc")

    $mapping = @{}
    foreach($row in $rows) {
        $sourcePath = $row.SourcePath
        if(-Not $mapping[$sourcePath]) {
            $mapping[$sourcePath] = @($row.FilePattern)
        } else {
            $mapping[$sourcePath] += $row.FilePattern
        }
    }

    return $mapping
}

function Get-RecoverFiles{
	param($dbConn)
	$rows = $dbConn.QueryTable("SELECT FileID, FullPath,
	            CASE WHEN CHARINDEX('_RCVR.',FileName) >0 THEN FileName 
	            ELSE REVERSE(SUBSTRING(REVERSE(FileName), CHARINDEX('.', REVERSE(FileName))+1, LEN(FileName)+1)) + '_RCVR' + '.' + FileType END AS NewFileName,
	            CASE WHEN CHARINDEX('_RCVR.',FileName) >0 THEN FullPath 
	            ELSE REPLACE(FullPath, FileName,REVERSE(SUBSTRING(REVERSE(FileName), CHARINDEX('.', REVERSE(FileName))+1, LEN(FileName)+1)) + '_RCVR' + '.' + FileType) END AS NewFullPath
            FROM
            (
	            SELECT fs.FileID, fa.FullPath,
	            REVERSE(SUBSTRING(REVERSE(fa.FullPath),0,CHARINDEX('_',REVERSE(fa.FullPath)))) AS RecoverTime,
	            REVERSE(SUBSTRING(REVERSE(fa.FullPath),0,CHARINDEX('\',REVERSE(fa.FullPath)))) AS FileName,
	            REVERSE(SUBSTRING(REVERSE(fa.FullPath),0,CHARINDEX('.',REVERSE(fa.FullPath)))) AS FileType
	            FROM etl.FilemoverStatus AS fs
	            INNER JOIN etl.FilemoverAvailableFiles AS fa
	            ON fs.FileID = fa.FileID
	            WHERE fs.Status = 'RECOVERED'
            ) AS file_set")
	foreach($row in $rows) {
		if(Test-Path -literalpath $row.FullPath) {			
			Rename-Item -LiteralPath $row.FullPath -NewName $row.NewFileName
			$fileId = $row.FileID
			$fullPath = $row.NewFullPath
			$dbConn.Execute("UPDATE etl.FilemoverStatus SET Status = 'RECOVER_IN_PROGRESS', UpdateDate = GETDATE() WHERE FileID = $FileId;
						  UPDATE etl.FilemoverAvailableFiles SET FullPath = '$fullPath', UpdateDate = GETDATE() WHERE FileID = $FileId")
						  
		}
		else {
			$fileId = $row.FileID
			$dbConn.Execute("UPDATE etl.FilemoverStatus SET Status = 'RECOVER_IN_PROGRESS' , UpdateDate = GETDATE() WHERE FileID = $FileId")
		}
	}
}

function FileMover {
	PARAM($dbConn, $dbName)
	$servers = Get-Servers -dbConn $dbConn
	enableSQLServerOption -sqlConn $dbConn -option 'Ole Automation Procedures' -value 1
    # Update Status from 'IN PROGRESS' to 'ERROR' (which means the prior run hanged or interrupted unexpectedly)
    Update-StatusInprogress2Error -dbConn $dbConn

	Log-Info "Find new files on servers"
	# logging
	Log-Info "File Mover In Progress"
    Write-Log $dbConn "FileMover" 9999 "File Mover Component" "IN PROGRESS"
    if(-Not $servers) {
        Log-Info "No active servers found"
        Log-Info "File Mover: No active servers"
        Write-Log $dbConn "FileMover" 9999 "No active servers" "INFO"
    }
	if(-Not $config) {
        $config  = Get-Config -connection $dbConn -dbName $dbName
    }
	
	Get-RecoverFiles -dbConn $dbConn
	
    foreach($server in $servers) {
		Log-Info "Checking Server: $($server.ServerName)"
        $src = $dbConn.QueryTable("SELECT * FROM metadata.FilemoverServer where ServerName = '$($server.ServerName)'")
        if($src.ServerType -notin @('SMB', 'FTP')) {
            Throw "FileMover supports only 'SMB', 'FTP' file movement. Current copytype $src.Type is not supported!"
        }
        # Search file recursively, support SMB server only
        $recursive = $src.Recursive
        # Get mapping between paths and file patterns
        $patternForPath = Get-ServerMapping -dbConn $dbConn -serverName $server.ServerName
        $files          = @()
        # go through all the directories, find applicable files
        foreach($path in $patternForPath.Keys) {
            $fullPath = Join-Path $server.RootDirectory $path
            Log-Info "`t$fullPath"
            if($src.ServerType -eq 'SMB'){
                $files += Find-NewFiles-SMB -path $fullPath -pattern $patternForPath[$path] -rename $server.Rename -config $config `
                            -recursive $recursive `
                            % | Add-Member SourcePath -MemberType NoteProperty -Value $path -PassThru
            }
            elseif($src.ServerType -eq 'FTP'){
                $srcPath = $fullPath.Replace('\','/')
                $files += Find-NewFiles-FTP -address $src.ServerAddress -username $src.Username -password $src.Password -srcPath $srcPath `
                            -pattern $patternForPath[$path] -rename $server.Rename -config $config `
                            % | Add-Member SourcePath -MemberType NoteProperty -Value $path -PassThru
            }
        }
        Log-Info "Matching files: $($files.count) file(s)"
        if($files.count -gt 0) {
            # insert the found files into available_files
            Update-Availability -dbConn $dbConn -files $files -serverName $server.ServerName
        }
    }
    Log-Info "`nFind files to upload"
    Copy-AllAvailableFiles -dbConn $dbConn -config $config
    Log-Info "Complete"
    Log-Info "File Mover Completed"
    Write-Log $dbConn "FileMover" 9999 "File Mover Component" "COMPLETED"
}