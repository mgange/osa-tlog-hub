. "$PSScriptRoot\..\common\Crypto.ps1"

function HasBeenRenamed {
    param($name)
    # match year month day hour (24) - minute second fff
    return $name -match "2[0-9]{3}[0-1][0-9][0-3][0-9]-[0-2][0-9][0-6][0-9][0-6][0-9][0-9]{3}"
}

function Add-TimeStamp {
    param($file)
    $baseName    = [System.IO.Path]::GetFileNameWithoutExtension($file)
    $extension   = [System.IO.Path]::GetExtension($file)
    $currentDate = Get-Date -format "yyyyMMdd-HHmmssfff"

    return "$($baseName)_$($currentDate)$($extension)"
}

function Is-Locked {
    param($file)
    try { [IO.File]::OpenWrite($file).close(); $false } catch { $true }
}

function RecentlyUpdated {
    # If the file was modified in the last X seconds we should not use it since it might be still be loading from another source
    param($file)
    return ((Get-Date) - $file.LastWriteTime).TotalSeconds -lt 60 #TODO make dynamic
}

function Find-NewFiles-SMB {
	param($path, $rename=$true, $patterns, $config, $recursive = 1)

    if(-Not $patterns) {
        Throw "Missing file patterns for $path"
    }
    $superPattern = ($patterns | % {"($_)"} ) -join '|'

    
    if($recursive){
	   $files = Get-ChildItem -Recurse $path
    } else{
        $files = Get-ChildItem $path
    }
    Log-Info "Found $($files.Count) file(s)"
    $ignored  = 0
    $newFiles = @()
    foreach($file in $files) {
        if($file.baseName -notmatch $superPattern -And $file.FullName -notmatch $superPattern) {
            Write-Verbose "ignore $file due to mismatched pattern '$superPattern' "
            $ignored++;
            continue;
        }
        if($rename -And (HasBeenRenamed $file)) {
            Write-Verbose "ignore $file due to timestamp"
            $ignored++;
            continue;
        }
        if(RecentlyUpdated $file) {
            Write-Verbose "ignore $file due to very recently modifition (less than 60s)"
            $ignored++;
            continue;
        }
        if(Is-Locked $file.FullName) {
            Write-Verbose "ignore $file due to being locked"
            $ignored++;
            continue;
        }
        if($rename) {
            $newName = Add-TimeStamp $file.Name
            Log-Info "Rename $file to $newName"
            $newFiles += Rename-Item $file.FullName -NewName $newName -PassThru
        } else {
            $newFiles += $file
        }
    }
    Log-Info "Ignored $ignored file(s)"
	
    return $newFiles
}

function Find-NewFiles-FTP {
    param($address, $username, $password, $srcPath, $patterns, $rename=$true)
    if(-Not $patterns) {
        Throw "Missing file patterns for $srcPath"
    }
    $sftp = New-Object Renci.SshNet.SftpClient($address, $username, (Decrypt $password))
    $maxTry = 5
    for($i = 0; $i -lt $maxTry;  $i++ ){
        try{
            $sftp.Connect();
            break
        }
        catch{
            if($i -eq $maxTry-1){
                throw
            }
            Start-Sleep -s $i
            Log-Info "the $i try to sftp shared node."
        }
    }

    if($sftp -eq $null){
        $msg = "the $maxTry times try to sftp to FTP server: $address using credential $username, quit the job!"
        Log-Info 
        throw $msg
    }

    $superPattern = ($patterns | % {"($_)"} ) -join '|'
    
    $files = $sftp.ListDirectory($srcPath)|Where {-not $_.IsDirectory}|Select Name, FullName, LastWriteTime, Length
    
    Log-Info "Found $($files.Count) file(s)"
    $ignored  = 0
    $newFiles = @()
    foreach($file in $files) {
        if($file.baseName -notmatch $superPattern -And $file.FullName -notmatch $superPattern) {
            Write-Verbose "ignore $file due to mismatched pattern '$superPattern' "
            $ignored++;
            continue;
        }
        if($rename -And (HasBeenRenamed $file)) {
            Write-Verbose "ignore $file due to timestamp"
            $ignored++;
            continue;
        }
        if(RecentlyUpdated $file) {
            Write-Verbose "ignore $file due to very recently modifition (less than 60s)"
            $ignored++;
            continue;
        }
        if($rename) {
            $newName = Add-TimeStamp $file.FullName
            $newFullName = $file.FullName.Replace($file.Name, $newName)
            Log-Info "Rename $file to $newName"
            $sftp.RenameFile($file.FullName, $newFullName)
            $file.Name = $newName
            $file.FullName = $newFullName
            $newFiles += $file
        } else {
            $newFiles += $file
        }
    }
    Log-Info "Ignored $ignored file(s)"
    
    return $newFiles
}