function Update-Availability {
    param($dbConn, $files, $serverName)

    $data = @()
    foreach($file in $files) {
        $data += @{
            server_name  = $serverName
            source_path  = $file.SourcePath
            full_path    = $file.FullName
            file_size    = $file.Length
            file_last_modified = $file.LastWriteTime.ToString('yyyy-MM-dd HH:mm:ss')
        }
    }

    $dbConn.Execute("IF OBJECT_ID('tempdb..#AVAILABLE_FILES_TMP') IS NOT NULL DROP TABLE #AVAILABLE_FILES_TMP;
                        SELECT ServerName, SourcePath, FullPath, FileSize, FileLastModified INTO #AVAILABLE_FILES_TMP FROM etl.FilemoverAvailableFiles WHERE 1 = 2;")
    [void]$dbConn.BatchInsertWithType("#AVAILABLE_FILES_TMP",
        "INSERT INTO #AVAILABLE_FILES_TMP(ServerName, SourcePath, FullPath, FileSize, FileLastModified) VALUES(@server_name, @source_path, @full_path, @file_size, @file_last_modified)",
        @(("server_name", "varchar", $null), ("source_path", "varchar", $null), ("full_path","varchar", $null), ("file_size", "numeric", $null), ("file_last_modified", "datetime", $null)),
        $data)

    $dbConn.Execute("INSERT INTO etl.FilemoverAvailableFiles(ServerName, SourcePath, FullPath, FileSize, FileLastModified) 
		SELECT * FROM #AVAILABLE_FILES_TMP tmp
        WHERE NOT EXISTS(
            SELECT top 1 NULL FROM etl.FilemoverAvailableFiles af
            WHERE af.ServerName        = tmp.ServerName
              AND af.SourcePath        = tmp.SourcePath
              AND af.FullPath          = tmp.FullPath
              AND af.FileLastModified = tmp.FileLastModified
              )")

}

function Update-StatusInprogress2Error {
    param($dbConn)
    $sql = "
		UPDATE etl.FilemoverStatus
        SET Status = 'ERROR', UpdateDate = GETDATE(),
        StatusReason = 'Reset FileID Status to ERROR, the last run of FileMover job was exit while still in progress or Recovering'
        WHERE Status in ('IN PROGRESS', 'RECOVER_IN_PROGRESS') ;
		"
    $dbConn.Execute($sql);
}