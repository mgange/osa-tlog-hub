. "$PSScriptRoot\..\common\Crypto.ps1"


function Add-TimeStamp {
    param($file)
    $baseName    = [System.IO.Path]::GetFileNameWithoutExtension($file)
    $extension   = [System.IO.Path]::GetExtension($file)
    $currentDate = Get-Date -format "yyyyMMdd-HHmmssfff"

    return "$($baseName)_$($currentDate)$($extension)"
}

function Is-Locked {
    param($file)
    try { [IO.File]::OpenWrite($file).close(); $false } catch { $true }
}

function DaysToKeep {
    # If the file was modified in the retention period
    param($file, $backupDaysToKeep)
    return $file.LastWriteTime -gt (Get-Date).AddDays(-$backupDaysToKeep)
}

function Find-Files-SMB {
	param($path, $patterns, $config, $backupDaysToKeep)

    if(-Not $patterns) {
        Throw "Missing file patterns for $path"
    }
    $superPattern = ($patterns | % {"($_)"} ) -join '|'

    
    $files = Get-ChildItem $path
    
    Log-Info "Found $($files.Count) file(s)"
    $ignored  = 0
    $newFiles = @()
    foreach($file in $files) {
        if($file.baseName -notmatch $superPattern -And $file.FullName -notmatch $superPattern) {
            Write-Verbose "ignore $file due to mismatched pattern '$superPattern' "
            $ignored++;
            continue;
        }
        if(DaysToKeep $file $backupDaysToKeep) {
            Write-Verbose "ignore $file due to retention period"
            $ignored++;
            continue;
        }
        if(Is-Locked $file.FullName) {
            Write-Verbose "ignore $file due to being locked"
            $ignored++;
            continue;
        }
        $newFiles += $file
    }
    Log-Info "Ignored $ignored file(s)"
	
    return $newFiles
}

function Find-Files-FTP {
    param($address, $username, $password, $srcPath, $patterns, $rename=$true, $backupDaysToKeep)
    if(-Not $patterns) {
        Throw "Missing file patterns for $srcPath"
    }
    $sftp = New-Object Renci.SshNet.SftpClient($address, $username, (Decrypt $password))
    $maxTry = 5
    for($i = 0; $i -lt $maxTry;  $i++ ){
        try{
            $sftp.Connect();
            break
        }
        catch{
            if($i -eq $maxTry-1){
                throw
            }
            Start-Sleep -s $i
            Log-Info "the $i try to sftp shared node."
        }
    }

    if($sftp -eq $null){
        $msg = "the $maxTry times try to sftp to FTP server: $address using credential $username, quit the job!"
        Log-Info 
        throw $msg
    }

    $superPattern = ($patterns | % {"($_)"} ) -join '|'
    
    $files = $sftp.ListDirectory($srcPath)|Where {-not $_.IsDirectory}|Select Name, FullName, LastWriteTime, Length
    
    Log-Info "Found $($files.Count) file(s)"
    $ignored  = 0
    $newFiles = @()
    foreach($file in $files) {
        if($file.baseName -notmatch $superPattern -And $file.FullName -notmatch $superPattern) {
            Write-Verbose "ignore $file due to mismatched pattern '$superPattern' "
            $ignored++;
            continue;
        }
        if($rename -And (HasBeenRenamed $file)) {
            Write-Verbose "ignore $file due to timestamp"
            $ignored++;
            continue;
        }
        if(DaysToKeep $file $backupDaysToKeep) {
            Write-Verbose "ignore $file due to retention period"
            $ignored++;
            continue;
        }
        $newFiles += $file
    }
    Log-Info "Ignored $ignored file(s)"
    
    return $newFiles
}