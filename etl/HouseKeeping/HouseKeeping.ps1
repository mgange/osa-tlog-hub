. "$PSScriptRoot\FindFiles.ps1"
. "$PSScriptRoot\..\common\common.ps1"
. "$PSScriptRoot\..\common\config.ps1"
. "$PSScriptRoot\..\common\Crypto.ps1"

$ErrorActionPreference = "Stop"

function Get-BackupFolders {
    param($dbConn)
    $rows = $dbConn.QueryTable("SELECT s.ServerName,s.ServerType,s.ServerAddress,s.PortNo,s.Username,s.Password,s.RootDirectory,m.FilePattern,m.BackupFolder,m.BackupDaysToKeep
                                from metadata.FilemoverMapping m INNER JOIN metadata.FilemoverServer s ON m.TargetServerName = s.ServerName
                                WHERE s.Active = 1 AND m.BackupFolder IS NOT NULL AND m.BackupFolder <> '' AND m.BackupDaysToKeep IS NOT NULL")
    return $rows
}

function RemoveFileFromSMBServer{
	param($fullPath)
	if(Test-Path -literalpath $fullPath){
        Log-Info "Deleting file: $fullPath"
		Remove-Item -literalpath $fullPath
	}
    else {
        Log-Info "Test-Path fail: $fullPath"
    }
}

function RemoveFileFromFTPServer{
    param($address, $username, $password, $fullPath)
    try{
        $sftp = New-Object Renci.SshNet.SftpClient($address, $username, (Decrypt $password))
        $sftp.Connect()
        $sftp.DeleteFile($fullPath)
    } catch {
        Log-Info "exception: $_"
        Throw
    } finally {
        $sftp.Disconnect();
    }
}



function HouseKeeping {
	PARAM($dbConn, $dbName)
	$backupFolders = Get-BackupFolders -dbConn $dbConn
	enableSQLServerOption -sqlConn $dbConn -option 'Ole Automation Procedures' -value 1

	Log-Info "Find old files"
	# logging
	Log-Info "HouseKeeping In Progress"
    Write-Log $dbConn "HouseKeeping" 9999 "HouseKeeping Component" "IN PROGRESS"
    if(-Not $backupFolders) {
        Log-Info "No active backup folders found"
        Log-Info "HouseKeeping: No active backup folders"
        Write-Log $dbConn "HouseKeeping" 9999 "No active backup folders" "INFO"
    }
	if(-Not $config) {
        $config  = Get-Config -connection $dbConn -dbName $dbName
    }
	
    foreach($backupFolder in $backupFolders) {
		Log-Info "Checking Folder: $($backupFolder.BackupFolder)"
        if($backupFolder.ServerType -notin @('SMB', 'FTP')) {
            Throw 'HouseKeeping supports only SMB, FTP file clean up. Current type is not supported!'
        }
        $files          = @()
        # go through all the directories, find applicable files
        $fullPath = $backupFolder.BackupFolder
        $pattern = $backupFolder.FilePattern
        Log-Info "File pattern: $pattern"
        if($backupFolder.ServerType -eq 'SMB'){
            $files += Find-Files-SMB -path $fullPath -pattern $pattern -backupDaysToKeep $backupFolder.BackupDaysToKeep -config $config

            Log-Info "Matching files: $($files.count) file(s)"

            if($files.count -gt 0) {
                foreach($file in $files) {
                    Log-Info "Matching file: $($file.FullName)"
                    RemoveFileFromSMBServer -fullPath $file.FullName
                }

            }
        }
        elseif($backupFolder.ServerType -eq 'FTP'){
            $srcPath = $fullPath.Replace('\','/')
            $files += Find-Files-FTP -address $backupFolder.ServerAddress -username $backupFolder.Username -password $backupFolder.Password -srcPath $srcPath `
                            -pattern $pattern -backupDaysToKeep $backupFolder.BackupDaysToKeep -config $config

            Log-Info "Matching files: $($files.count) file(s)"

            if($files.count -gt 0) {
                foreach($file in $files) {
                    Log-Info "Matching file: $($file.FullName)"
                    RemoveFileFromFTPServer -address $server.ServerAddress -username $backupFolder.Username -password $backupFolder.Password -fullPath $file.FullName
                }
            }
        }
    }
    Log-Info "Complete"
    Log-Info "HouseKeeping Completed"
    Write-Log $dbConn "HouseKeeping" 9999 "HouseKeeping Component" "COMPLETED"
}