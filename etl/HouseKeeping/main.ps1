Param(
   $dbServer=".\sql2014"
 , $dbName="HUB_POC"
)
# Load libraries
. "$PSScriptRoot\..\common\common.ps1"
. "$PSScriptRoot\..\common\log.ps1"
. "$PSScriptRoot\..\common\connect.ps1"
. "$PSScriptRoot\Housekeeping.ps1"

try {
    $dbConn = Create-SqlConnection $dbServer $dbName
    $config  = Get-Config -connection $dbConn -dbName $dbName
    $logDir  = $config.GetProperty("etl.logs.dir", "$PSScriptRoot\..\logs")
    $logDate = Get-Date -Format "yyyyMMdd"
    Write-Log $dbConn "Housekeeping" 9999 "Housekeeping Component" "STARTED"

    UnRegister-AllLoggers
    Register-Log @("info", "warn", "err") 'Log-StdOut'
    Register-Log @("debug","info", "warn", "err") 'Log-File' @{dir=$logDir; name="HOUSEKEEPING_$dbName`_$logDate"}
    
    Log-Info "Housekeeping Started"
    Housekeeping -dbConn $dbConn -dbName $dbName.toUpper()

    # indicate success to the SQL Agent job
    Exit 0
} catch {
    if($dbConn) {
        Write-Log $dbConn 'Housekeeping' $audit.loadId "exception $_"
    }
    Print-Error
    # indicate failure to the SQL Agent job
    Exit 1
} finally {
    if($dbConn -And $dbConn.IsOpen) {
        $dbConn.close()
    }
}